$("#cancelCreateUndangan").on('click', function () {
	window.location.href = url + '/login/logout';
})

$("#subdomain").on('input', function () {
	let subDomain = $(this).val();
	const cekDomain = $(".cekDomain");
	if (subDomain.length > 0) {
		cekDomain.show();
	} else {
		cekDomain.hide();
	}
	cekDomain.html(``);
	cekDomain.html(`<span class="text-primary">Mengecek Sub Domain</span>`);
})

// konvert ke rupiah
function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++)
		if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

// show password
$(".toggle-password").click(function () {
	$(this).find(".mdi").toggleClass("mdi-eye mdi-eye-off");
	var input = $($(this).attr("toggle"));
	if (input.attr("type") == "password") {
		input.attr("type", "text");
	} else {
		input.attr("type", "password");
	}
});

// clear modal
function kosongkanModal() {
	const title = $(".modal-content").find(".modal-title");
	const body = $(".modal-content").find(".modal-body");

	title.html("");
	body.html("");
}

$("#cekPaket").on("click", function () {
	const title = $(".modal-content").find(".modal-title");
	const body = $(".modal-content").find(".modal-body");

	kosongkanModal();

	title.html("Cek Paket");
	const paketId = $(this).data("paket");
	$.ajax({
		url: url + 'ajax/cek_paket',
		type: 'post',
		dataType: 'json',
		data: {
			'paket_id': paketId
		},
		success: function (success) {
			if (success.status == "1") {
				console.log(success.data);
				const content = `
				<dl class="row">
					<dt class="col-sm-4">Nama Paket</dt>
					<dd class="col-sm-8">${success.data.nama_paket}</dd>
				</dl>
				<dl class="row">
					<dt class="col-sm-4">Maksimal Tamu</dt>
					<dd class="col-sm-8">${success.data.max_tamu}</dd>
				</dl>
				<dl class="row">
					<dt class="col-sm-4">Deskripsi</dt>
					<dd class="col-sm-8">${success.data.deskripsi}</dd>
				</dl>
				<dl class="row">
					<dt class="col-sm-4">Harga</dt>
					<dd class="col-sm-8">${convertToRupiah(success.data.harga)}</dd>
				</dl>
				`
				body.html(content);
			} else {
				body.html(success.msg);
			}
		}
	})
})

// preview foto pengantin pria
function previewFotoPp() {
	const foto = document.querySelector("#foto_pp");
	const imgPreview = document.querySelector('.fotopp-preview');

	const fileFoto = new FileReader();
	fileFoto.readAsDataURL(foto.files[0]);

	fileFoto.onload = function (e) {
		imgPreview.src = e.target.result;
	}
}

// preview foto pengantin wanita
function previewFotoPw() {
	const foto = document.querySelector("#foto_pw");
	const imgPreview = document.querySelector('.fotopw-preview');

	const fileFoto = new FileReader();
	fileFoto.readAsDataURL(foto.files[0]);

	fileFoto.onload = function (e) {
		imgPreview.src = e.target.result;
	}
}
