// Login 
$(document).ready(function () {
	$('#logForm').submit(function (e) {
		e.preventDefault();
		$('#logText').html('<i class="mdi mdi-loading mdi-spin"></i>&nbsp; Checking...');
		var user = $('#logForm').serialize();
		var login = function () {
			$.ajax({
				type: 'POST',
				url: url + '/login/do_login',
				dataType: 'json',
				data: user,
				success: function (response) {
					$('#message').html(response.message);
					$('#logText').html('Login');
					if (response.error) {
						$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
						$('#email').addClass("is-invalid");
						$('#password').addClass("is-invalid");
					} else {
						$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
						// $('#logForm')[0].reset();
						$('#email').removeClass("is-invalid");
						$('#password').removeClass("is-invalid");
						$('#email').addClass("is-valid");
						$('#password').addClass("is-valid");
						$('.toggle-password').hide();
						setTimeout(function () {
							location.reload();
						}, 3000);
					}
				}
			});
		};
		setTimeout(login, 3000);
	});
})

$("#cekTagihan").on("click", function () {
	$("#modal").find("#modal-title").html("Cek Tagihan");
	$("#modal").find("#modal-body").html(`<input type="text" onkeypress="hanyaAngka()" placeholder="Masukkan Kode Transaksi" class="form-control" id="kode_transaksi">`);
	$("#modal").find("#modal-submit").html("Kirim");
	$("#modal").find("#modal-submit").addClass("cekTagihan");
});

$('#modal').on('click', '.cekTagihan', function (e) {
	const kodeTransaksi = $("#kode_transaksi").val();
	$("#modal-body").find(".alert").remove();
	$(this).attr("disabled", "disabled");
	$(this).html("Tunggu Sebentar");
	$.ajax({
		url: url + 'ajax/cek_kode_transaksi',
		type: 'post',
		dataType: 'json',
		data: {
			'kode_transaksi': kodeTransaksi
		},
		success: function (success) {
			if (success.status == "1") {
				$("#modal-body").append(`<div class="alert alert-success mt-2">Kode Transaksi Ditemukan</div>`);
				$(".cekTagihan").html("Sedang Mendirect Ke Halaman Tagihan");
				setInterval(() => {
					window.location.href = url + 'tagihan/' + kodeTransaksi;
				}, 1500);
			} else {
				$("#modal-body").append(`<div class="alert alert-danger mt-2">Kode Transaksi Tidak Ditemukan</div>`);
				$(".cekTagihan").removeAttr("disabled");
				$(".cekTagihan").html("Kirim")
			}
		}
	})
});

// validasi hanya angka
function hanyaAngka(evt) {
	var theEvent = evt || window.event;

	// Handle paste
	if (theEvent.type === 'paste') {
		key = event.clipboardData.getData('text/plain');
	} else {
		// Handle key press
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode(key);
	}
	var regex = /[0-9]|\./;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

function cekPromo(){
	const kode	=	$("#kodePromo").val();
	const paket =	$("#paket").val();
	const body	=	$("#msgPromo");
	if(kode.length > 0){
		$.ajax({
			url: url + 'ajax/cek_promo',
			type: 'post',
			dataType: 'json',
			data: {
				'kode': kode,
				'paket': paket
			},
			success: function (success) {
				if (success.status == "1") {
					body.removeClass("badge-danger");
					body.addClass("badge-success");
					body.html(success.msg);
				} else {
					body.removeClass("badge-success");
					body.addClass("badge-danger");
					body.html(success.msg);
				}
			}
		})
	}else{
		body.html("");
	}
}

$("#kodePromo").keyup(function () {
	cekPromo();
})

$("#paket").change(function () {
	cekPromo();
})

// konvert ke rupiah
function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++)
		if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}