<?php $this->load->view('frontend/inc/head_html'); ?>
<!-- <div id="responseDiv">
							<div id="message"></div>
						</div>
						<h4 class="text-center">Masuk</h4>
						<span class="text-center text-muted d-block">Belum punya akun Sipardi? <a href="<?= base_url('register'); ?>">Daftar.</a></span> -->
<!-- <form class="mt-3 px-3" method="POST" id="logForm" autocomplete="on">
							<div class="form-group">
								<label for="email">Email address</label>
								<input type="email" class="form-control" id="email" placeholder="name@example.com" name="email" required autofocus>
							</div>
							<div class="form-group">
								<label for="password">Password</label>
								<input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
								<span toggle="#password" class="text-primary fas fa-eye field-icon toggle-password"></span>
							</div>
							<div class="form-group">
								<small class="d-block text-muted text-right"><a href="<?= base_url("forget"); ?>">Lupa Katasandi</a></small>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-outline-primary btn-block" id="logText">Masuk</button>
							</div>
						</form> -->

<!-- Loader -->
<div id="preloader">
	<div id="status">
		<div class="spinner">
			<div class="double-bounce1"></div>
			<div class="double-bounce2"></div>
		</div>
	</div>
</div>
<!-- Loader -->

<div class="back-to-home rounded d-none d-sm-block">
	<a href="<?= base_url(); ?>" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- Hero Start -->
<section class="bg-home d-flex align-items-center">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-lg-7 col-md-6">
				<div class="mr-lg-5">
					<img src="<?= base_url(); ?>assets/images/user/login.svg" class="img-fluid d-block mx-auto" alt="">
				</div>
			</div>
			<div class="col-lg-5 col-md-6">
				<div class="card login-page bg-white shadow rounded border-0">
					<div class="card-body">

						<div id="responseDiv">
							<div id="message"></div>
						</div>

						<h4 class="card-title text-center">Login</h4>
						<?= $this->session->userdata("user"); ?>
						<form class="login-form mt-4" method="POST" id="logForm" autocomplete="on">
							<div class="row">
								<div class="col-lg-12">
									<div class="form-group position-relative">
										<label>Your Email <span class="text-danger">*</span></label>
										<i data-feather="user" class="fea icon-sm icons"></i>
										<input type="email" class="form-control pl-5" placeholder="Email" name="email" id="email" required="" autofocus>
									</div>
								</div>

								<div class="col-lg-12">
									<div class="form-group position-relative">
										<label>Password <span class="text-danger">*</span></label>
										<i data-feather="key" class="fea icon-sm icons"></i>
										<input type="password" class="form-control pl-5" placeholder="Password" name="password" id="password" required="">
									</div>
								</div>

								<div class="col-lg-12">
									<div class="d-flex justify-content-between">
										<!-- <div class="form-group">
											<div class="custom-control custom-checkbox">
												<input type="checkbox" class="custom-control-input" id="customCheck1">
												<label class="custom-control-label" for="customCheck1">Remember me</label>
											</div>
										</div> -->
										<p class="forgot-pass mb-0"><a href="<?= base_url("forget"); ?>" class="text-dark font-weight-bold">Lupa password ?</a></p>
									</div>
								</div>
								<div class="col-lg-12 mb-0">
									<button type="submit" class="btn btn-primary btn-block" id="logText">Masuk</button>
								</div>

								<div class="col-12 text-center">
									<p class="mb-0 mt-3"><small class="text-dark mr-2">Don't have an account ?</small> <a href="<?= base_url("register"); ?>" class="text-dark font-weight-bold">Sign Up</a></p>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!---->
			</div>
			<!--end col-->
		</div>
		<!--end row-->
	</div>
	<!--end container-->
</section>
<!--end section-->
<!-- Hero End -->


<?php $this->load->view('frontend/inc/foot_html'); ?>