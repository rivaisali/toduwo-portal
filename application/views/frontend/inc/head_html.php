<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Toduwo - Website dan Undangan Pernikahan Digital</title>
    <script>
        const url = '<?php echo base_url(); ?>';
    </script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Premium Bootstrap 4 Landing Page Template" />
    <meta name="keywords" content="Saas, Software, multi-uses, HTML, Clean, Modern" />
    <meta name="author" content="Shreethemes" />
    <meta name="email" content="shreethemes@gmail.com" />
    <meta name="website" content="http://www.shreethemes.in" />
    <meta name="Version" content="v2.6" />
    <!-- favicon -->
    <link rel="shortcut icon" href="<?= base_url(); ?>/assets/images/favicon.ico">
    <!-- Bootstrap -->
    <link href="<?= base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Magnific -->
    <link href="<?= base_url(); ?>/assets/css/magnific-popup.css" rel="stylesheet" type="text/css" />
    <!-- Icons -->
    <link href="<?= base_url(); ?>/assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.3/css/line.css">
    <!-- Slider -->               
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/owl.carousel.min.css"/> 
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/css/owl.theme.default.min.css"/> 
    <!-- Main Css -->
    <link href="<?= base_url(); ?>/assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" />
    <link href="<?= base_url(); ?>/assets/css/colors/default.css" rel="stylesheet" id="color-opt">
<style>
    * {margin: 0; padding: 0}
    html {
        scroll-behavior: smooth;
    }
</style>
</head>

<body>