   <!-- Navbar STart -->
   <header id="topnav" class="defaultscroll sticky">
        <div class="container">
            <!-- Logo container-->
            <div>
                <a class="logo" href="index.html">
                    <img src="<?= base_url(); ?>/assets/images/logo.svg" height="80" alt="">
                </a>
            </div>                 
            <ul class="buy-button list-inline mb-0">
              
                <!-- <li class="list-inline-item mb-0">
                    <a href="javascript:void(0)" class="btn btn-primary mr-2" data-toggle="modal" data-target="#productview">Masuk</a>
                </li> -->
                <li class="list-inline-item mb-0">
                    <a href="javascript:void(0)" class="btn btn-icon btn-soft-primary">
                        <i class="mdi mdi-facebook mdi-18px icons"></i></a>&nbsp;
                        
                </li>
                <li class="list-inline-item mb-0">
                    <a href="javascript:void(0)" class="btn btn-icon btn-soft-success">
                        <i class="mdi mdi-instagram mdi-18px icons"></i></a>   
                </li>&nbsp;
                <li class="list-inline-item mb-0">
                    <a href="javascript:void(0)" class="btn btn-icon btn-soft-danger">
                        <i class="mdi mdi-play-circle mdi-18px icons"></i></a>&nbsp;
                        
                </li>
            </ul><!--end login button-->
            <!-- End Logo container-->
            <div class="menu-extras">
                <div class="menu-item">
                    <!-- Mobile menu toggle-->
                    <a class="navbar-toggle">
                        <div class="lines">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                    </a>
                    <!-- End mobile menu toggle-->
                </div>
            </div>
    
            <div id="navigation">
                <!-- Navigation Menu-->   
                <ul class="navigation-menu">
                    <li><a href="#home">Home</a></li>
                    <li class="has-submenu">
                        <a href="#profil">Profil</a> 
                    </li>
                    <li class="has-submenu">
                        <a href="#harga">Harga</a> 
                    </li>
                    <li class="has-submenu">
                        <a href="#fitur">Fitur</a> 
                    </li>
                    <li class="has-submenu">
                        <a href="javascript:void(0)">Portofolio</a> 
                    </li>
                
                </ul><!--end navigation menu-->
                <div class="buy-menu-btn d-none">
                    <li class="list-inline-item mb-0">
                        <div class="dropdown">
                            <button type="button" class="btn btn-link text-decoration-none dropdown-toggle p-0 pr-2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="mdi mdi-magnify h4 text-muted"></i>
                            </button>
                            <div class="dropdown-menu dd-menu dropdown-menu-right bg-white shadow rounded border-0 mt-3 py-0" style="width: 240px;">
                                <form>
                                    <input type="text" id="text" name="name" class="form-control border bg-white" placeholder="Search...">
                                </form>
                            </div>
                         </div>
                    </li>
                    <li class="list-inline-item mb-0 pr-1">
                        <a href="#" class="btn btn-icon btn-soft-primary"><i class="mdi mdi-github mdi-18px icons"></i></a>
                    </li>
                    <li class="list-inline-item mb-0 pr-1">
                        <a href="#" class="btn btn-icon btn-soft-primary"><i class="mdi mdi-stack-overflow mdi-18px icons"></i></a>
                    </li>
                    <li class="list-inline-item mb-0">
                        <a href="javascript:void(0)" class="btn btn-icon btn-soft-primary" data-toggle="modal" data-target="#productview"><i class="mdi mdi-account-outline mdi-18px icons"></i></a>
                    </li>
                </div><!--end login button-->
            </div><!--end navigation-->
        </div><!--end container-->
    </header><!--end header-->
<!--end header-->
<!-- Navbar End -->