
    <!-- Back to top -->
    <a href="#" class="btn btn-icon btn-primary back-to-top"><i data-feather="arrow-up" class="icons"></i></a>
    <!-- Back to top -->


    <!-- javascript -->
    <script src="<?= base_url(); ?>/assets/js/jquery-3.5.1.min.js"></script>
    <script src="<?= base_url(); ?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>/assets/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>/assets/js/scrollspy.min.js"></script>
    <!-- SLIDER -->
    <script src="<?= base_url(); ?>/assets/js/owl.carousel.min.js "></script>
    <script src="<?= base_url(); ?>/assets/js/owl.init.js "></script>
    <!-- Icons -->
    <script src="<?= base_url(); ?>/assets/js/feather.min.js"></script>
    <script src="https://unicons.iconscout.com/release/v3.0.3/script/monochrome/bundle.js"></script>
    <!-- Switcher -->
    <script src="<?= base_url(); ?>/assets/js/switcher.js"></script>
    <!-- Main Js -->
    
    <script src="<?= base_url(); ?>/assets/js/app.js"></script>
     <script src="<?= base_url(); ?>/assets/js/custom.js"></script>
</body>
</html>