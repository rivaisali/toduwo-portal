
    <!-- Loader -->
    <!-- <div id="preloader">
        <div id="status">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div> -->
    <!-- Loader -->

 
    <!-- Navbar End -->

    <!-- Hero Start -->
    <section class="bg-half bg-light d-table w-100" id="home">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6 col-12">
                    <div class="title-heading">
                        <span class="badge badge-pill badge-soft-primary">Promo Tahun Baru 2021</span>
                        <h1 class="font-weight-bold mt-2 mb-3">Website &<br>  Undangan Digital</h1>
                        <p class="para-desc text-muted">Platform Undangan Digital Terlengkap untuk Solusi Aman mengirim undangan saat pandemi Covid-19.</p>
                        <div class="mt-4 pt-2">
                            <a href="<?= base_url("register"); ?>" class="btn btn-primary mr-2">Daftar Sekarang!</a>
                            <a href="javascript:void(0)" class="btn btn-outline-primary">Lihat Portofolio</a>
                        </div>
                        <!-- <p class="text-muted mb-0 mt-3">Current Version: v2.6.0</p> -->
                    </div>
                </div><!--end col-->

                <div class="col-md-6 mt-4 pt-2 mt-sm-0 pt-sm-0">
                    <img src="<?= base_url(); ?>/assets/images/banner.svg" class="img-fluid d-block mx-auto" alt="">
                </div><!--end col-->
            </div><!--end row-->
        </div> <!--end container-->
    </section><!--end section-->

    <!-- Partners start -->
    <section class="py-4 border-top bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                    <img src="<?= base_url(); ?>/assets/images/client/amazon.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->

                <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                    <img src="<?= base_url(); ?>/assets/images/client/google.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
                
                <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                    <img src="<?= base_url(); ?>/assets/images/client/lenovo.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
                
                <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                    <img src="<?= base_url(); ?>/assets/images/client/paypal.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
                
                <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                    <img src="<?= base_url(); ?>/assets/images/client/shopify.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
                
                <div class="col-lg-2 col-md-2 col-6 text-center py-4">
                    <img src="<?= base_url(); ?>/assets/images/client/spotify.svg" class="avatar avatar-ex-sm" alt="">
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->
    </section><!--end section-->
    <!-- Partners End -->

    <div class="position-relative">
        <div class="shape overflow-hidden text-white">
            <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
            </svg>
        </div>
    </div>
    <!-- Hero End -->

    <!-- Start -->
    <section class="section" id="profil">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <span class="badge badge-pill badge-soft-primary">Work Process</span>
                        <h4 class="title mt-3 mb-4">Tentang Kami ?</h4>
                        <p class="text-muted para-desc mb-0 mx-auto"><span class="text-primary font-weight-bold">Toduwo.id </span>adalah Platform undangan digital. Fitur yang didalamnya dibuat mudah dan lengkap untuk mengurangi adanya kontak fisik di era new normal ini.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-md-4 col-12 mt-5">
                    <div class="features text-center">
                        <div class="image position-relative d-inline-block">
                            <i class="uil uil-scenery h1 text-primary"></i>
                        </div>

                        <div class="content mt-4">
                            <h5>Desain Modern</h5>
                            <p class="text-muted mb-0">Undangan yang disediakan memiliki desain yang kekinian dan modern  </p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-4 col-12 mt-5">
                    <div class="features text-center">
                        <div class="image position-relative d-inline-block">
                            <i class="uil uil-adjust-circle h1 text-primary"></i>
                        </div>

                        <div class="content mt-4">
                            <h5>Flexibel dan Interaktif</h5>
                            <p class="text-muted mb-0">Undangan yang sangat interaktif dan mudah digunakan ketika pertama kali dilihat</p>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-md-4 col-12 mt-5">
                    <div class="features text-center">
                        <div class="image position-relative d-inline-block">
                            <i class="uil uil-clock h1 text-primary"></i>
                        </div>

                        <div class="content mt-4">
                            <h5>Layanan 24/7</h5>
                            <p class="text-muted mb-0">Pelayanan 24/7 jam dari kami untuk client.</p>
                        </div>
                    </div>
                </div><!--end col-->

                <!-- <div class="col-12 mt-5 text-center">
                    <a href="javascript:void(0)" class="btn btn-pills btn-primary">See More</a>
                </div> -->
            </div><!--end row-->
        </div><!--end container-->
        <div class="container mt-100 mt-60" id="fitur">
            <div class="row">
                <div class="col-12">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">Fitur</h4>
                        <p class="para-desc text-muted mb-0">Kami <span class="text-primary font-weight-bold">Toduwo.ID</span> Memberikan fitur-fitur yang membuat undangan anda terlihat sempurna.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row align-items-center">
                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card border-0 features feature-clean course-feature p-4 overflow-hidden shadow">
                        <div class="icons text-primary text-center">
                            <i class="uil uil-layer-group d-block rounded h3 mb-0"></i>
                        </div>
                        <div class="card-body p-0 mt-4">                                            
                            <a href="javascript:void(0)" class="title h5 text-dark">Undangan Pribadi</a>
                            <p class="text-muted mt-2">Undangan Digital Dikirimkan kepada Personal langsung melalui whatsapp.</p>
                            <!-- <a href="javascript:void(0)" class="text-primary read-more">Read More <i class="mdi mdi-chevron-right"></i></a> -->
                            <i class="uil uil-layer-group text-primary full-img"></i>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card border-0 features feature-clean course-feature p-4 overflow-hidden shadow">
                        <div class="icons text-primary text-center">
                            <i class="uil uil-airplay d-block rounded h3 mb-0"></i>
                        </div>
                        <div class="card-body p-0 mt-4">                                            
                            <a href="javascript:void(0)" class="title h5 text-dark">Undangan Web Digital</a>
                            <p class="text-muted mt-2">Undangan digital itu dapat di unduh atau dapat di view lewat halaman website yang sudah disiapkan
                            </p>
                            <!-- <a href="javascript:void(0)" class="text-primary read-more">Read More <i class="mdi mdi-chevron-right"></i></a> -->
                            <i class="uil uil-airplay text-primary full-img"></i>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card border-0 features feature-clean course-feature p-4 overflow-hidden shadow">
                        <div class="icons text-primary text-center">
                            <i class="uil uil-focus-target d-block rounded h3 mb-0"></i>
                        </div>
                        <div class="card-body p-0 mt-4">                                            
                            <a href="javascript:void(0)" class="title h5 text-dark">Buku Tamu Digital</a>
                            <p class="text-muted mt-2">Para Tamu dapat mengisi buku tamu secara digital dengan cara mengarahkan QrCode Undangannya kepada Tablet/PC yang ada di Penerima Tamu
                            </p>
                            <!-- <a href="javascript:void(0)" class="text-primary read-more">Read More <i class="mdi mdi-chevron-right"></i></a> -->
                            <i class="uil uil-focus-target text-primary full-img"></i>
                        </div>
                    </div>
                </div><!--end col-->

                <div class="col-lg-3 col-md-6 mt-4 pt-2">
                    <div class="card border-0 features feature-clean course-feature p-4 overflow-hidden shadow">
                        <div class="icons text-primary text-center">
                            <i class="uil uil-expand-arrows d-block rounded h3 mb-0"></i>
                        </div>
                        <div class="card-body p-0 mt-4">                                            
                            <a href="javascript:void(0)" class="title h5 text-dark">Digital Gift</a>
                            <p class="text-muted mt-2">Para Tamu dapat mengirimkan Gift secara Langsung dengan Klik Halaman Gift Kado pada undangan digital
                            </p>
                            <!-- <a href="javascript:void(0)" class="text-primary read-more">Read More <i class="mdi mdi-chevron-right"></i></a> -->
                            <i class="uil uil-expand-arrows text-primary full-img"></i>
                        </div>
                    </div>
                </div><!--end col-->
            </div><!--end row-->
        </div><!--end container-->

    



        <div class="container mt-100 mt-60" id="harga">
            <div class="row justify-content-center">
                <div class="col-12">
                    <div class="section-title text-center mb-4 pb-2">
                        <h4 class="title mb-4">Paket</h4>
                        <p class="para-desc mx-auto text-muted mb-0">Harga bersahabat sesuai dengan kebutuhanmu
                            Semua paket sudah mendapatkan halaman website, manajemen tamu, RSVP, komentar, dan buku tamu digital!.</p>
                    </div>
                </div><!--end col-->
            </div><!--end row-->

            <div class="row">
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="card pricing-rates business-rate border-0 p-4 rounded-md shadow">
                        <div class="card-body p-0">
                            <span class="py-2 px-4 d-inline-block bg-soft-primary h6 mb-0 text-primary rounded-lg">FREE</span>
                            <h2 class="font-weight-bold mb-0 mt-3">Rp. 0</h2>
                            <p class="text-muted">Sekali Beli</p>

                            <p class="text-muted">Paket undangan gratis untuk para kalian yang mau mencoba.</p>

                            <ul class="list-unstyled mb-0 pl-0">
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>25 Undangan</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>1 Subdomain <span class="text-danger small">free.toduwo.id</span></li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>1 Pilihan Desain <span class="text-danger small">default</span></li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                            </ul>

                            <div class="mt-4">
                                <a href="<?= base_url("register"); ?>" class="btn btn-primary mt-4">Daftar</a>
                                <!-- <p class="text-muted mt-3 mb-0">*No credit card required</p> -->
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="card pricing-rates business-rate border-0 p-4 rounded-md shadow">
                        <div class="ribbon ribbon-right ribbon-warning overflow-hidden"><span class="text-center d-block shadow small h6">Best</span></div>
                        <div class="card-body p-0">
                            <span class="py-2 px-4 d-inline-block bg-soft-primary h6 mb-0 text-primary rounded-lg">GOLD</span>
                            <h2 class="font-weight-bold mb-0 mt-3">Rp. 150K</h2>
                            <p class="text-muted">Sekali Beli</p>

                            <p class="text-muted">Paket Gold untuk anda yang ingin membuat undangan dengan 1 tema yang beragam.</p>

                            <ul class="list-unstyled pt-3 border-top">
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>150 Undangan</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>1 Subdomain <span class="text-danger small">request</span></li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>1 Pilihan Desain</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li></ul>

                            <div class="mt-4">
                                <a href="<?= base_url("register"); ?>" class="btn btn-primary mt-4">Daftar</a>
                                <p class="text-muted mt-3 mb-0">*No credit card required</p>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->
                
                <div class="col-lg-4 col-md-6 mt-4 pt-2">
                    <div class="card pricing-rates business-rate border-0 p-4 rounded-md shadow">
                        <div class="card-body p-0">
                            <span class="py-2 px-4 d-inline-block bg-soft-primary h6 mb-0 text-primary rounded-lg">PLATINUM</span>
                            <h2 class="font-weight-bold mb-0 mt-3">Rp. 300K</h2>
                            <p class="text-muted">Sekali Beli</p>

                            <p class="text-muted">Paket Gold untuk anda yang ingin membuat undangan dengan tema yang beragam.</p>

                            <ul class="list-unstyled pt-3 border-top">
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>350 Undangan</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>1 Subdomain <span class="text-danger small">request</span></li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>3 Pilihan Desain</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2"><i class="uil uil-check-circle align-middle"></i></span>Buku Tamu Digital <span class="text-danger small">1 User</span></li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                                <li class="h6 text-muted mb-0"><span class="text-primary h5 mr-2">&nbsp;</li>
                                </ul>

                            <div class="mt-4">
                                <a href="<?= base_url("register"); ?>" class="btn btn-primary mt-4">Daftar</a>
                                <p class="text-muted mt-3 mb-0">*No credit card required</p>
                            </div>
                        </div>
                    </div>
                </div><!--end col-->

                
            </div><!--end row-->
        </div><!--end container-->
        <div class="container-fluid mt-100 mt-60">
            <div class="bg-primary rounded shadow py-5" style="background: url('<?= base_url(); ?>/assets/images/cta.png') center center;">
                <div class="container my-md-5">
                    <div class="row">
                        
                        <div class="col-12 text-center">
                            <div class="section-title">
                                <h4 class="title text-white title-dark mb-4">Tunggu apa lagi? Miliki sekarang juga!</h4>
                                <p class="text-white-50 para-desc mb-0 mx-auto"></p>
                                <center>
                                    <form>
                                        <div class="form-group col-7 mt-4 pt-3 mb-0">
                                            <div class="input-group">
                                                <input name="text" id="domain" maxlength="35" type="email" 
                                                style="background: white;border:none; height: 70px;font-size: x-large;text-align:right;" class="form-control" 
                                                placeholder="Subdomain" required="" aria-describedby="newssubscribebtn">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" style="background: white;border:none; height: 70px;font-size: x-large;text-align:right;font-weight: bold;color:currentColor;" basic-addon2">.toduwo.id</span>
                                                   
                                                  </div>
                                                <div class="input-group-append">
                                                    <button class="btn submitBnt" style="background-color: #ebda0e;font-size:larger;font-weight: bold;" type="submit" id="paypalmail">Miliki Sekarang</button>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </form>
                                </center>
                                
                            </div>
                        </div><!--end col-->
                    </div><!--end row-->
                </div><!--end container-->
            </div><!--end div-->
        </div><!--end container-->
        <!-- <div class="container mt-100 mt-60">
            <div class="row justify-content-center">
                <div class="col-12 text-center">
                    <div class="section-title mb-4 pb-2">
                        <h4 class="title mb-4">Client's Review</h4>
                        <p class="text-muted para-desc mx-auto mb-0">Start working with <span class="text-primary font-weight-bold">Landrick</span> that can provide everything you need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-12 mt-4">
                    <div id="customer-testi" class="owl-carousel owl-theme">
                        <div class="media customer-testi m-2">
                            <img src="<?= base_url(); ?>/assets/images/client/01.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" It seems that only fragments of the original text remain in the Lorem Ipsum texts used today. "</p>
                                <h6 class="text-primary">- Thomas Israel <small class="text-muted">C.E.O</small></h6>
                            </div>
                        </div>
                        
                        <div class="media customer-testi m-2">
                            <img src="<?= base_url(); ?>/assets/images/client/02.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star-half text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" One disadvantage of Lorum Ipsum is that in Latin certain letters appear more frequently than others. "</p>
                                <h6 class="text-primary">- Barbara McIntosh <small class="text-muted">M.D</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="<?= base_url(); ?>/assets/images/client/03.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" The most well-known dummy text is the 'Lorem Ipsum', which is said to have originated in the 16th century. "</p>
                                <h6 class="text-primary">- Carl Oliver <small class="text-muted">P.A</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="<?= base_url(); ?>/assets/images/client/04.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" According to most sources, Lorum Ipsum can be traced back to a text composed by Cicero. "</p>
                                <h6 class="text-primary">- Christa Smith <small class="text-muted">Manager</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="<?= base_url(); ?>/assets/images/client/05.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" There is now an abundance of readable dummy texts. These are usually used when a text is required. "</p>
                                <h6 class="text-primary">- Dean Tolle <small class="text-muted">Developer</small></h6>
                            </div>
                        </div>

                        <div class="media customer-testi m-2">
                            <img src="<?= base_url(); ?>/assets/images/client/06.jpg" class="avatar avatar-small mr-3 rounded shadow" alt="">
                            <div class="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul class="list-unstyled mb-0">
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                    <li class="list-inline-item"><i class="mdi mdi-star text-warning"></i></li>
                                </ul>
                                <p class="text-muted mt-2">" Thus, Lorem Ipsum has only limited suitability as a visual filler for German texts. "</p>
                                <h6 class="text-primary">- Jill Webb <small class="text-muted">Designer</small></h6>
                            </div>
                        </div>
                    </div>
                </div>
        </div> -->
    </section><!--end section-->
    <!-- End -->
  

    <!-- Modal Content Start -->
    <div class="modal fade" id="productview" tabindex="-1" role="dialog" aria-labelledby="productview-title" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content rounded shadow-lg border-0 overflow-hidden">
                <div class="modal-body p-0">
                    <div class="container-fluid px-0">
                        <div class="row align-items-center no-gutters">
                            <div class="col-lg-6 col-md-5">
                                <img src="<?= base_url(); ?>/assets/images/course/online/ab02.jpg" class="img-fluid" alt="">
                            </div><!--end col-->

                            <div class="col-lg-6 col-md-7">
                                <form class="login-form p-4">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Your Email <span class="text-danger">*</span></label>
                                                <div class="position-relative">
                                                    <i data-feather="user" class="fea icon-sm icons"></i>
                                                    <input type="email" class="form-control pl-5" placeholder="Email" name="email" required="">
                                                </div>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label>Password <span class="text-danger">*</span></label>
                                                <div class="position-relative">
                                                    <i data-feather="key" class="fea icon-sm icons"></i>
                                                    <input type="password" class="form-control pl-5" placeholder="Password" required="">
                                                </div>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-lg-12">
                                            <div class="d-flex justify-content-between">
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                        <label class="custom-control-label" for="customCheck1">Remember me</label>
                                                    </div>
                                                </div>
                                                <p class="forgot-pass mb-0"><a href="auth-re-password.html" class="text-dark font-weight-bold">Forgot password ?</a></p>
                                            </div>
                                        </div><!--end col-->

                                        <div class="col-lg-12 mb-0">
                                            <button class="btn btn-primary btn-block">Sign in</button>
                                        </div><!--end col-->

                                        <div class="col-12 text-center">
                                            <p class="mb-0 mt-3"><small class="text-dark mr-2">Don't have an account ?</small> <a href="auth-signup.html" class="text-dark font-weight-bold">Sign Up</a></p>
                                        </div><!--end col-->
                                    </div><!--end row-->
                                </form>
                            </div><!--end col-->
                        </div><!--end row-->
                    </div><!--end container-->
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Content End -->
