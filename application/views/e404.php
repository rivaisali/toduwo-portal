<?php $this->load->view('frontend/inc/head_html'); ?>
<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="double-bounce1"></div>
            <div class="double-bounce2"></div>
        </div>
    </div>
</div>
<!-- Loader -->

<div class="back-to-home rounded d-none d-sm-block">
    <a href="index.html" class="btn btn-icon btn-soft-primary"><i data-feather="home" class="icons"></i></a>
</div>

<!-- MAINTENANCE PAGE -->
<section class="bg-home d-flex align-items-center" data-jarallax='{"speed": 0.5}' style="background-image: url('<?= base_url("assets/images/maintenance.jpg") ?>');">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-md-12 text-center">
                <a href="javascript:void(0)" class="logo h5"><img src="<?= base_url(); ?>assets/images/logo-light.png" height="24" alt=""></a>
                <div class="text-uppercase text-white title-dark mt-2 mb-4 maintenance">Page Not Found</div>
                <p class="text-white-50 para-desc mx-auto para-dark">Halaman yang anda kunjungi tidak tersedia.</p>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-center">
                <a href="<?= base_url(); ?>" class="btn btn-primary mt-4"><i class="mdi mdi-backup-restore"></i> Go Back Home</a>
            </div>
        </div>
    </div> <!-- end container -->
</section>
<!--end section-->
<!-- MAINTENANCE PAGE -->
<?php $this->load->view('frontend/inc/foot_html'); ?>