<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?></div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Status</th>
                            <th>Email</th>
                            <th>No. Telp</th>
                            <th>Tgl Daftar</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($data as $d) :
                            if ($d->status == "0") {
                                $badge  =   "danger";
                                $msg    =   "Nonaktif";
                            } else {
                                $badge  =   "success";
                                $msg    =   "Aktif";
                            }
                        ?>
                            <tr class="odd gradeX">
                                <td>
                                    <?= $d->nama; ?>
                                </td>
                                <td>
                                    <small class="badge badge-<?= $badge ?>"><?= $msg; ?></small>
                                </td>
                                <td>
                                    <?= $d->email; ?>
                                </td>
                                <td>
                                    <?= $d->no_telp; ?>
                                </td>
                                <td>
                                    <?= tgl_full($d->created_at); ?>
                                </td>
                                <td class="actions text-center">
                                    <a class="btn btn-primary" href="<?= base_url($base . "/detail/" . $d->user_id); ?>">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>