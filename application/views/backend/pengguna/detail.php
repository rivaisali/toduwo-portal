<?php
if ($data->status == "0") {
    $color          =   "danger";
    $color_toggle   =   "success";
    $msg_toggle     =   "Aktifkan";
    $link           =   "verifikasi";
} else {
    $color          =   "success";
    $color_toggle   =   "danger";
    $msg_toggle     =   "Nonaktifkan";
    $link           =   "blokir";
}
?>
<div class="row">
    <div class="col-12">
        <div class="card card-border-color card-border-color-<?= $color; ?>">
            <div class="card-body">
                <div class="row p-2">
                    <div class="col-12 col-lg-6">
                        <dl class="row">
                            <dt class="col-12 col-lg-3">Nama</dt>
                            <dd class="col-12 col-lg-9"><?= $data->nama; ?></dd>

                            <dt class="col-12 col-lg-3">Email</dt>
                            <dd class="col-12 col-lg-9"><?= $data->email; ?></dd>

                            <dt class="col-12 col-lg-3">Telp</dt>
                            <dd class="col-12 col-lg-9"><?= $data->no_telp; ?></dd>

                            <dt class="col-12 col-lg-3">Kode Transaksi</dt>
                            <dd class="col-12 col-lg-9"><?= $data->kode_transaksi; ?></dd>

                            <dt class="col-12 col-lg-3">Paket</dt>
                            <dd class="col-12 col-lg-9"><?= ambil_nama_by_id("paket", "nama_paket", "paket_id", $data->paket_id); ?></dd>

                            <dt class="col-12 col-lg-3">Tgl Daftar</dt>
                            <dd class="col-12 col-lg-9"><?= tgl_full($data->created_at); ?></dd>
                        </dl>
                        <a href="<?= base_url($base . "/" . $link . "/" . $data->user_id); ?>" class="btn btn-<?= $color_toggle; ?>"><?= $msg_toggle; ?></a>
                    </div>
                    <div class="col-12 col-lg-6"></div>
                </div>
            </div>
        </div>

        <div class="card card-border-color card-border-color-primary">
            <div class="card-header">
                <div class="card-title">
                    Data Undangan
                </div>
            </div>
            <div class="card-body">
                <div class="row p-2">
                    <?php if ($undangan) : ?>
                        <div class="col-12">
                            <dl class="row">
                                <dt class="col-12 col-lg-3">Desain Undangan</dt>
                                <dd class="col-12 col-lg-9"><?= ambil_nama_by_id("desain", "judul_desain", "desain_id", $undangan->desain_id); ?></dd>

                                <dt class="col-12 col-lg-3">Judul</dt>
                                <dd class="col-12 col-lg-9"><?= $undangan->judul; ?></dd>

                                <dt class="col-12 col-lg-3">Domain</dt>
                                <dd class="col-12 col-lg-9"><?= $undangan->nama_domain; ?></dd>

                                <dt class="col-12 col-lg-3">Nama Pengantin Pria</dt>
                                <dd class="col-12 col-lg-9"><?= $undangan->nama_pengantin_pria; ?></dd>

                                <dt class="col-12 col-lg-3">Foto Pengantin Pria</dt>
                                <dd class="col-12 col-lg-9">
                                    <a target="_blank" href="<?= base_url("uploads/pengantin/" . $undangan->foto_pengantin_pria); ?>">Lihat Foto</a>
                                </dd>

                                <dt class="col-12 col-lg-3">Tentang Pengantin Pria</dt>
                                <dd class="col-12 col-lg-9"><?= $undangan->tentang_pengantin_pria; ?></dd>

                                <dt class="col-12 col-lg-3">Nama Pengantin Wanita</dt>
                                <dd class="col-12 col-lg-9"><?= $undangan->nama_pengantin_wanita; ?></dd>

                                <dt class="col-12 col-lg-3">Foto Pengantin Wanita</dt>
                                <dd class="col-12 col-lg-9">
                                    <a target="_blank" href="<?= base_url("uploads/pengantin/" . $undangan->foto_pengantin_wanita); ?>">Lihat Foto</a>
                                </dd>

                                <dt class="col-12 col-lg-3">Tentang Pengantin Wanita</dt>
                                <dd class="col-12 col-lg-9"><?= $undangan->tentang_pengantin_wanita; ?></dd>
                            </dl>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <div class="card card-border-color card-border-color-primary">
            <div class="card-header">
                <div class="card-title">
                    Data Acara
                </div>
            </div>
            <div class="card-body">
                <div class="row p-2">
                    <?php if ($acara) : ?>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nama Acara</th>
                                    <th>Tanggal / Jam</th>
                                    <th>Lokasi</th>
                                    <th>Alamat</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($acara as $ac) : ?>
                                    <tr>
                                        <td><?= $ac->nama_acara; ?></td>
                                        <td><?= tgl_full($ac->tanggal . " " . $ac->jam); ?></td>
                                        <td><?= $ac->nama_lokasi; ?></td>
                                        <td><?= $ac->alamat_lokasi; ?></td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>