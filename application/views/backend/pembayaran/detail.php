<div class="row">
    <div class="col-12">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <div class="row p-2">
                    <div class="col-12 col-lg-6">
                        <dl class="row">
                            <dt class="col-12 col-lg-3">Nama Pengguna</dt>
                            <dd class="col-12 col-lg-9"><?= $user->nama; ?></dd>

                            <dt class="col-12 col-lg-3">Email</dt>
                            <dd class="col-12 col-lg-9"><?= $user->email; ?></dd>

                            <dt class="col-12 col-lg-3">Telp</dt>
                            <dd class="col-12 col-lg-9"><?= $user->no_telp; ?></dd>

                            <dt class="col-12 col-lg-3">Kode Promo</dt>
                            <dd class="col-12 col-lg-9"><?= $user->kode_promo; ?></dd>

                            <dt class="col-12 col-lg-3">Tagihan</dt>
                            <dd class="col-12 col-lg-9"><?= rupiah($data->tagihan); ?></dd>

                            <dt class="col-12 col-lg-3">Bank Pengirim</dt>
                            <dd class="col-12 col-lg-9"><?= $data->bank_pengirim; ?></dd>

                            <dt class="col-12 col-lg-3">Nama Pengirim</dt>
                            <dd class="col-12 col-lg-9"><?= $data->nama_pengirim; ?></dd>

                            <dt class="col-12 col-lg-3">Nominal</dt>
                            <dd class="col-12 col-lg-9"><?= rupiah($data->nominal); ?></dd>

                            <dt class="col-12 col-lg-3">Bank Tujuan</dt>
                            <dd class="col-12 col-lg-9"><?= $data->bank_tujuan; ?></dd>

                            <dt class="col-12 col-lg-3">Waktu Upload</dt>
                            <dd class="col-12 col-lg-9"><?= tgl_indonesia($data->create_at); ?></dd>

                            <dt class="col-12 col-lg-3">Batas Bayar</dt>
                            <dd class="col-12 col-lg-9"><?= tgl_indonesia($user->batas_bayar); ?></dd>

                            <dt class="col-12 col-lg-3">Bukti Bayar</dt>
                            <dd class="col-12 col-lg-9">
                                <a target="_blank" href="<?= base_url("uploads/bukti_bayar/" . $data->bukti_bayar); ?>">
                                    <img class="img-fluid" src="<?= base_url("uploads/bukti_bayar/" . $data->bukti_bayar); ?>" alt="Bukti Bayar">
                                </a>
                            </dd>
                        </dl>
                        <?php if ($data->status == "1") { ?>
                            <a href="<?= base_url($base . "/konfirmasi/verifikasi/" . $data->kode_transaksi); ?>" class="btn btn-success">Verifikasi</a>
                            <a href="<?= base_url($base . "/konfirmasi/tolak/" . $data->kode_transaksi); ?>" class="btn btn-danger">Batalkan</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>