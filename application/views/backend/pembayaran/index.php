<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?></div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Status</th>
                            <th>Tgl Bayar</th>
                            <th>Nominal</th>
                            <th>Pengirim</th>
                            <th>Bank Tujuan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $d) :
                            if ($d->status == "1") {
                                $badge  =   "primary";
                                $msg    =   "Pending";
                            } elseif ($d->status == "0") {
                                $badge  =   "danger";
                                $msg    =   "Dibatalkan";
                            } else {
                                $badge  =   "success";
                                $msg    =   "Sudah Diverifikasi";
                            }
                        ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td>
                                    <small class="badge badge-<?= $badge ?>"><?= $msg; ?></small>
                                </td>
                                <td>
                                    <?= tgl_laporan($d->create_at); ?>
                                </td>
                                <td><?= rupiah($d->nominal); ?></td>
                                <td>
                                    <?= $d->bank_pengirim . "<br>" . $d->nama_pengirim; ?>
                                </td>
                                <td><?= $d->bank_tujuan; ?></td>
                                <td class="actions text-center">
                                    <a class="btn btn-primary" href="<?= base_url($base . "/detail/" . $d->kode_transaksi); ?>">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>