<?php $this->load->view('backend/inc/head_html'); ?>
<div class="bg-home d-flex align-items-center">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-md-12 fuelux">
				<h2 class="text-center text-primary font-weight-bold my-4">Lengkapi Form Undangan Untuk Memulai.</h2>
				<form action="<?= base_url("backend/create_undangan"); ?>" method="POST" enctype="multipart/form-data">
					<div class="block-wizard">
						<div class="wizard wizard-ux" id="wizard1">
							<div class="steps-container">
								<ul class="steps">
									<li class="active" data-step="1">Step 1<span class="chevron"></span></li>
									<li data-step="2">Step 2<span class="chevron"></span></li>
									<li data-step="3">Step 3<span class="chevron"></span></li>
								</ul>
							</div>
							<div class="actions">
								<button class="btn btn-xs btn-prev btn-secondary" type="button"><i class="icon mdi mdi-chevron-left"></i> Prev</button>
								<button class="btn btn-xs btn-next btn-secondary" type="button" data-last="Tekan Complete Untuk Selesai">Next<i class="icon mdi mdi-chevron-right"></i></button>
							</div>
							<div class="step-content">
								<div class="step-pane active" data-step="1">
									<div class="container p-0">
										<div class="form-group row">
											<div class="col-sm-7">
												<h3 class="wizard-title">Data Acara</h3>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="card card-contrast">
													<div class="card-body">
														<div class="form-group pt-0">
															<label for="judul">Judul Acara <i class="text-danger">*</i></label>
															<input class="form-control form-control-sm <?= form_error('judul') ? 'is-invalid' : ''; ?>" id="judul" type="text" placeholder="Judul / Tema Acara" name="judul" value="<?= set_value('judul', ''); ?>">
															<?= form_error('judul'); ?>
														</div>
														<div class="form-group pt-0">
															<label for="subdomain">Subdomain <i class="text-danger">*</i></label>
															<div class="input-group mb-3">
																<input class="form-control form-control-sm <?= form_error('subdomain') ? 'is-invalid' : ''; ?>" id="subdomain" type="text" placeholder="Subdomain" name="subdomain" value="<?= set_value('subdomain', ''); ?>">
																<div class="input-group-append"><span class="input-group-text form-control form-control-sm">.toduwo.id</span></div>
																<?= form_error('subdomain'); ?>
															</div>
															<div class="cekDomain" style="display: none;"></div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="card card-contrast">
													<div class="card-body">
														<div class="form-group pt-0">
															<label for="nama_acara">Nama Acara <i class="text-danger">*</i></label>
															<input class="form-control form-control-sm <?= form_error('nama_acara') ? 'is-invalid' : ''; ?>" id="nama_acara" type="text" placeholder="Resepsi / Akad Nikah" name="nama_acara" value="<?= set_value('nama_acara', ''); ?>">
															<?= form_error('nama_acara'); ?>
														</div>

														<div class="form-group pt-0">
															<label for="tgl">Tanggal dan Jam <i class="text-danger">*</i></label>
															<div class="input-group date datetimepicker" data-start-view="0" data-date="" data-date-format="yyyy-mm-dd - HH:ii" data-link-field="dtp_input1">
																<div class="input-group-append input-group-sm">
																	<button class="btn btn-primary"><i class="icon-th mdi mdi-calendar"></i></button>
																</div>
																<input name="tgl" id="tgl" class="form-control form-control-sm <?= form_error('tgl') ? 'is-invalid' : ''; ?>" size="16" type="text" value="<?= set_value('tgl', ''); ?>">
																<?= form_error('tgl'); ?>
															</div>
														</div>

														<div class="form-group pt-0">
															<label for="lokasi">Lokasi <i class="text-danger">*</i></label>
															<input class="form-control form-control-sm <?= form_error('lokasi') ? 'is-invalid' : ''; ?>" id="lokasi" type="text" placeholder="Lokasi Acara" name="lokasi" value="<?= set_value('lokasi', ''); ?>">
															<?= form_error('lokasi'); ?>
														</div>

														<div class="form-group pt-0">
															<label for="alamat">Alamat Lokasi <i class="text-danger">*</i></label>
															<input class="form-control form-control-sm <?= form_error('alamat') ? 'is-invalid' : ''; ?>" id="alamat" type="text" placeholder="Alamat Lokasi" name="alamat" value="<?= set_value('alamat', ''); ?>">
															<?= form_error('lokasi'); ?>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<button class="btn btn-secondary btn-space" type="button" id="cancelCreateUndangan">Cancel</button>
												<button class="btn btn-primary btn-space wizard-next" data-wizard="#wizard1">Next Step</button>
											</div>
										</div>
									</div>
								</div>
								<div class="step-pane" data-step="2">
									<div class="container p-0">
										<div class="form-group row">
											<div class="col-sm-7">
												<h3 class="wizard-title">Data Pengantin</h3>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class="card card-contrast">
													<div class="card-header card-header-contrast card-header-featured">
														Pengantin Pria
													</div>
													<div class="card-body">
														<div class="form-group pt-0">
															<label for="nama_pp">Nama Pengantin Pria <i class="text-danger">*</i></label>
															<input class="form-control form-control-sm <?= form_error('nama_pp') ? 'is-invalid' : ''; ?>" id="nama_pp" type="text" placeholder="Nama Pengantin Pria" name="nama_pp" value="<?= set_value('nama_pp', ''); ?>">
															<?= form_error('nama_pp'); ?>
														</div>
														<div class="form-group pt-0">
															<label for="deskripsi_pp">Tentang Pengantin Pria <i class="text-danger">*</i></label>
															<textarea class="form-control form-control-sm <?= form_error('deskripsi_pp') ? 'is-invalid' : ''; ?>" id="deskripsi_pp" placeholder="Contoh: Anak Pertama Dari Bapak Fullan." name="deskripsi_pp"><?= set_value('deskripsi_pp', ''); ?></textarea>
															<?= form_error('deskripsi_pp'); ?>
														</div>
														<div class="form-group pt-0">
															<label for="foto_pp">Foto Pengantin Pria</label>
															<div class="input-group mb-3">
																<div class="custom-file">
																	<input class="custom-file-input <?= form_error('foto_pp') ? 'is-invalid' : ''; ?>" id="foto_pp" type="file" aria-describedby="foto_pp" name="foto_pp">
																	<label class="custom-file-label" for="foto_pp">Pilih Foto</label>
																	<?= form_error('foto_pp', ''); ?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="card card-contrast">
													<div class="card-header card-header-contrast card-header-featured">
														Pengantin Wanita
													</div>
													<div class="card-body">
														<div class="form-group pt-0">
															<label for="nama_pw">Nama Pengantin Wanita <i class="text-danger">*</i></label>
															<input class="form-control form-control-sm <?= form_error('nama_pw') ? 'is-invalid' : ''; ?>" id="nama_pw" type="text" placeholder="Nama Pengantin Wanita" name="nama_pw" value="<?= set_value('nama_pw', ''); ?>">
															<?= form_error('nama_pw'); ?>
														</div>
														<div class="form-group pt-0">
															<label for="deskripsi_pw">Tentang Pengantin Wanita <i class="text-danger">*</i></label>
															<textarea class="form-control form-control-sm <?= form_error('deskripsi_pw') ? 'is-invalid' : ''; ?>" id="deskripsi_pw" placeholder="Contoh: Anak Pertama Dari Bapak Fullan." name="deskripsi_pw"><?= set_value('deskripsi_pw', ''); ?></textarea>
															<?= form_error('deskripsi_pw'); ?>
														</div>
														<div class="form-group pt-0">
															<label for="foto_pw">Foto Pengantin Wanita</label>
															<div class="input-group mb-3">
																<div class="custom-file">
																	<input class="custom-file-input form-control <?= form_error('foto_pw') ? 'is-invalid' : ''; ?>" id="foto_pw" type="file" aria-describedby="foto_pw" name="foto_pw">
																	<label class="custom-file-label" for="foto_pw">Pilih Foto</label>
																	<?= form_error('foto_pw', ''); ?>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="form-group row">
											<div class="col-sm-12">
												<button class="btn btn-secondary btn-space wizard-previous" data-wizard="#wizard1">Previous</button>
												<button class="btn btn-primary btn-space wizard-next" data-wizard="#wizard1">Next Step</button>
											</div>
										</div>
									</div>
								</div>
								<div class="step-pane" data-step="3">
									<div class="form-group row">
										<div class="col-sm-7">
											<h3 class="wizard-title">Pilih Tema Undangan</h3>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-12">
											<div class="row">
												<?php foreach ($desain as $d) : ?>
													<div class="col-lg-4 bg-secondary p-1">
														<div class="card mb-0">
															<img class="card-img-top" src="<?= base_url("uploads/desain/" . $d->gambar); ?>" alt="Placeholder">
															<div class="card-header"><?= $d->judul_desain; ?></div>
															<div class="card-body">
																<p><?= $d->keterangan; ?></p>
																<label class="custom-control custom-radio">
																	<input class="custom-control-input custom-control-sm" type="radio" name="desain" value="<?= $d->desain_id; ?>" <?= set_radio('desain', $d->desain_id); ?> <?= ($d->desain_id == "1") ? 'checked' : ''; ?>><span class="custom-control-label">Pilih Desain</span>
																</label>
																<a class="card-link" href="<?= $d->link_demo; ?>">Link Demo</a>
															</div>
														</div>
													</div>
												<?php endforeach; ?>
											</div>
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-12">
											<button class="btn btn-secondary btn-space wizard-previous" data-wizard="#wizard1">Previous</button>
											<button type="submit" class="btn btn-success btn-space">Complete</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php $this->load->view('backend/inc/foot_html'); ?>