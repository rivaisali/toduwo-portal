<script src="<?= base_url(); ?>assets_admin/lib/jquery/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/perfect-scrollbar/js/perfect-scrollbar.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/bootstrap/dist/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/js/app.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>assets_admin/lib/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>assets_admin/lib/fuelux/js/wizard.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/select2/js/select2.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/bootstrap-slider/bootstrap-slider.min.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>assets_admin/lib/bs-custom-file-input/bs-custom-file-input.js" type="text/javascript"></script>

<!-- datatables -->
<script src="<?= base_url(); ?>assets_admin/lib/datatables/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/datatables/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/datatables/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets_admin/lib/datatables/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function() {
        //-initialize the javascript
        App.init();
        App.wizard();
        App.formElements();
        App.dataTables();
    });
</script>
<!-- <script src="<?= base_url(); ?>assets_admin/js/jquery.validate.js" type="text/javascript"></script> -->
<script src="<?= base_url(); ?>assets_admin/js/custom.js" type="text/javascript"></script>
</body>

</html>