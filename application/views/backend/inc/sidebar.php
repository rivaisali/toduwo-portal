<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class=""><a href="<?= base_url("backend"); ?>"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a></li>

                        <?php if (user("level") == "user") { ?>
                            <li class=""><a href="<?= base_url("group_tamu"); ?>"><i class="icon mdi mdi-accounts-alt"></i><span>Grup Tamu</span></a></li>
                            <li class=""><a href="<?= base_url("tamu"); ?>"><i class="icon mdi mdi-male-female"></i><span>Tamu</span></a></li>
                            <li class=""><a href="<?= base_url("penerima_tamu"); ?>"><i class="icon mdi mdi-face"></i><span>Penerima Tamu</span></a></li>
                            <li class="parent"><a href="#"><i class="icon mdi mdi-settings"></i><span>Pengaturan</span></a>
                                <ul class="sub-menu">
                                    <li><a href="<?= base_url("pengaturan/pengantin"); ?>">Data Pengantin</a></li>
                                    <li><a href="<?= base_url("pengaturan/acara"); ?>">Data Acara</a></li>
                                    <li><a href="<?= base_url("pengaturan/akun"); ?>">Akun</a></li>
                                </ul>
                            </li>
                            <li class="divider">Features</li>
                            <li><a href="#"><i class="icon mdi mdi-whatsapp"></i><span>Hubungi Kami</span></a>
                            </li>
                        <?php } else { ?>
                            <li class="divider">Master</li>
                            <li class="">
                                <a href="<?= base_url("paket"); ?>">
                                    <i class="icon mdi mdi-labels"></i>Paket
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= base_url("desain"); ?>">
                                    <i class="icon mdi mdi-collection-image-o"></i>Desain Undangan
                                </a>
                            </li>
                            <li class="divider">User</li>
                            <li class="">
                                <a href="<?= base_url("pengguna"); ?>">
                                    <i class="icon mdi mdi-accounts"></i><span class="badge badge-primary float-right" id="countPenggunaBaru"></span>Pengguna
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= base_url("pembayaran"); ?>">
                                    <i class="icon mdi mdi-money-box"></i><span class="badge badge-primary float-right" id="countPembayaranBaru"></span>Pembayaran
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="progress-widget">
            <div class="progress-data"><span class="progress-value">60%</span><span class="name">Current Project</span></div>
            <div class="progress">
                <div class="progress-bar progress-bar-primary" style="width: 60%;"></div>
            </div>
        </div> -->
    </div>
</div>