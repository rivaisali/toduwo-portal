<?php if (user("level") == "user") { ?>
    <div class="row">
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="widget widget-tile text-primary">
                <div class="chart">
                    <span class="mdi mdi-accounts-alt" style="font-size: 3rem;"></span>
                </div>
                <div class=" data-info">
                    <div class="desc">Grup Tamu</div>
                    <div class="value">
                        <?= $grup_tamu; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="widget widget-tile text-success">
                <div class="chart">
                    <span class="mdi mdi-male-female" style="font-size: 3rem;"></span>
                </div>
                <div class=" data-info">
                    <div class="desc">Tamu</div>
                    <div class="value">
                        <?= $tamu; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="widget widget-tile text-info">
                <div class="chart">
                    <span class="mdi mdi-face" style="font-size: 3rem;"></span>
                </div>
                <div class=" data-info">
                    <div class="desc">Penrima Tamu</div>
                    <div class="value">
                        <?= $penerima_tamu; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-xl-3">
            <div class="widget widget-tile text-danger">
                <div class="chart">
                    <span class="mdi mdi-cake" style="font-size: 3rem;"></span>
                </div>
                <div class=" data-info">
                    <div class="desc">Acara</div>
                    <div class="value text-truncate" title="<?= $undangan->judul; ?>">
                        <?= $undangan->judul; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-lg-6 col-md-6">
            <div class="row">
                <div class="col-12 col-lg-6 col-md-6">
                    <div class="card mb-3">
                        <img src="<?= base_url("uploads/pengantin/" . $undangan->foto_pengantin_pria); ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"><?= $undangan->nama_pengantin_pria; ?></h5>
                            <p class="card-text"><?= $undangan->tentang_pengantin_pria; ?></p>
                            <p class="card-text"><small class="text-muted">Pengantin Pria</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-md-6">
                    <div class="card mb-3">
                        <img src="<?= base_url("uploads/pengantin/" . $undangan->foto_pengantin_wanita); ?>" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h5 class="card-title"><?= $undangan->nama_pengantin_wanita; ?></h5>
                            <p class="card-text"><?= $undangan->tentang_pengantin_wanita; ?></p>
                            <p class="card-text"><small class="text-muted">Pengantin Wanita</small></p>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <a href="<?= base_url("pengaturan/pengantin"); ?>" class="btn btn-block btn-primary">Ubah Data Pengantin</a>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-md-6">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header"><?= $undangan->judul; ?></div>
                <div class="card-body">
                    <dl class="row">
                        <dt class="col-sm-3">Tanggal</dt>
                        <dd class="col-sm-9"><?= tgl_indonesia($acara->tanggal); ?></dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-3">Jam</dt>
                        <dd class="col-sm-9"><?= jam($acara->tanggal . " " . $acara->jam); ?></dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-3">Lokasi</dt>
                        <dd class="col-sm-9"><?= $acara->nama_lokasi; ?></dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-3">Alamat Lokasi</dt>
                        <dd class="col-sm-9"><?= $acara->alamat_lokasi; ?></dd>
                    </dl>

                    <hr class="row" />

                    <dl class="row">
                        <dt class="col-sm-3">Paket</dt>
                        <dd class="col-sm-9">
                            <a href="#" id="cekPaket" data-paket="<?= $user->paket_id; ?>" data-toggle="modal" data-target="#modal" type="button" class="text-decoration-none">
                                <?= ambil_nama_by_id("paket", "nama_paket", "paket_id", $user->paket_id); ?>
                            </a>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-3">Domain</dt>
                        <dd class="col-sm-9">
                            <a href="<?= "https://" . $undangan->nama_domain . ".toduwo.id" ?>" class="text-decoration-none" target="_blank">
                                <?= "https://" . $undangan->nama_domain . ".toduwo.id"; ?>
                            </a>
                        </dd>
                    </dl>
                    <dl class="row">
                        <dt class="col-sm-3">Desain Undangan</dt>
                        <dd class="col-sm-9">
                            <a href="<?= ambil_nama_by_id("desain", "link_demo", "desain_id", $undangan->desain_id); ?>" class="text-decoration-none" target="_blank">
                                <?= ambil_nama_by_id("desain", "judul_desain", "desain_id", $undangan->desain_id); ?>
                            </a>
                        </dd>
                    </dl>

                    <hr class="row" />

                    <span class="text-muted">Untuk perubahan paket, silahkan hubungi kami.</span>
                    <hr class="row" />
                    <a href="<?= base_url("pengaturan/acara"); ?>" class="btn btn-block btn-primary">Ubah Data Acara</a>
                </div>
            </div>
        </div>
    </div>
<?php } else { // admin 
?>
    <div class="row d-flex">
        <div class="col-12 col-lg-6 col-md-6">
            <div class="row">
                <div class="col-12 col-lg-6 col-md-12 col-sm-12">
                    <div class="widget widget-tile text-primary">
                        <div class="chart">
                            <span class="mdi mdi-labels" style="font-size: 3rem;"></span>
                        </div>
                        <div class=" data-info">
                            <div class="desc">Paket</div>
                            <div class="value">
                                <?= $paket; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-md-12 col-sm-12">
                    <div class="widget widget-tile text-danger">
                        <div class="chart">
                            <span class="mdi mdi-collection-image-o" style="font-size: 3rem;"></span>
                        </div>
                        <div class=" data-info">
                            <div class="desc">Desain Undangan</div>
                            <div class="value">
                                <?= $desain; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-md-12 col-sm-12">
                    <div class="widget widget-tile text-info">
                        <div class="chart">
                            <span class="mdi mdi-accounts-alt" style="font-size: 3rem;"></span>
                        </div>
                        <div class=" data-info">
                            <div class="desc">Pengguna</div>
                            <div class="value">
                                <?= $pengguna; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-6 col-md-12 col-sm-12">
                    <div class="widget widget-tile text-success">
                        <div class="chart">
                            <span class="mdi mdi-money-box" style="font-size: 3rem;"></span>
                        </div>
                        <div class=" data-info">
                            <div class="desc">Total Pemasukkan</div>
                            <div class="value">
                                <?= rupiah($pemasukkan); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-lg-6 col-md-6">
            <div class="card card-border-color card-border-color-primary">
                <div class="card-header">Pembayaran Yang Masih Pending</div>
                <div class="card-body p-0">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Tgl Bayar</th>
                                <th>Nominal</th>
                                <th>Kode Transaksi</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (empty($bayar_pending)) {
                                echo '
                                <tr><td colspan="4">
                                    <div class="alert alert-info alert-simple" role="alert">
                                        <div class="icon"> <span class="mdi mdi-info-outline"></span></div>
                                        <div class="message"><strong>Info!</strong> Belum ada data.</div>
                                    </div>
                                </td></tr>
                                ';
                            } else {
                                foreach ($bayar_pending as $bp) :
                            ?>
                                    <tr>
                                        <td><?= tgl_laporan($bp->create_at); ?></td>
                                        <td><?= rupiah($bp->nominal); ?></td>
                                        <td><?= $bp->kode_transaksi; ?></td>
                                        <td>
                                            <a href="<?= base_url("pembayaran/detail/" . $bp->kode_transaksi); ?>" class="btn btn-primary btn-sm">Detail</a>
                                        </td>
                                    </tr>
                            <?php
                                endforeach;
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<?php } ?>