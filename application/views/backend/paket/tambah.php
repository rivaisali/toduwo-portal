<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <div class="form-group pt-2">
                    <label for="namaPaket">Nama Paket</label>
                    <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="namaPaket" type="text" placeholder="Masukkan Nama Paket" value="<?= set_value('nama', ''); ?>" autofocus>
                    <?= form_error('nama'); ?>
                </div>
                <div class="form-group pt-2">
                    <label for="keteranganPaket">Deskripsi</label>
                    <textarea name="deskripsi" id="keteranganPaket" class="form-control form-control-sm"><?= set_value('deskripsi', ''); ?></textarea>
                </div>
                <div class="form-group pt-2">
                    <label for="maxTamu">Maximal Tamu</label>
                    <input class="form-control form-control-sm <?= form_error('max_tamu') ? 'is-invalid' : ''; ?>" name="max_tamu" id="maxTamu" type="text" placeholder="Maximal Tamu" value="<?= set_value('max_tamu', ''); ?>">
                    <?= form_error('max_tamu'); ?>
                </div>
                <div class="form-group pt-2">
                    <label for="penerimaTamu">Penerima Tamu</label>
                    <input class="form-control form-control-sm <?= form_error('user_penerima_tamu') ? 'is-invalid' : ''; ?>" name="user_penerima_tamu" id="penerimaTamu" type="text" placeholder="Penerima Tamu" value="<?= set_value('user_penerima_tamu', ''); ?>">
                    <?= form_error('user_penerima_tamu'); ?>
                </div>
                <div class="form-group pt-2">
                    <label for="harga">Harga</label>
                    <input class="form-control form-control-sm <?= form_error('harga') ? 'is-invalid' : ''; ?>" name="harga" id="harga" type="text" placeholder="Harga" value="<?= set_value('harga', ''); ?>">
                    <?= form_error('harga'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>