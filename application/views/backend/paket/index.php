<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Paket</th>
                            <th>Deskripsi</th>
                            <th>Max Tamu</th>
                            <th>Penerima Tamu</th>
                            <th>Harga</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td><?= $d->nama_paket; ?></td>
                                <td>
                                    <?= $d->deskripsi; ?>
                                </td>
                                <td>
                                    <?= $d->max_tamu; ?>
                                </td>
                                <td>
                                    <?= $d->user_penerima_tamu; ?>
                                </td>
                                <td>
                                    <?= rupiah($d->harga); ?>
                                </td>
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->paket_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1" href="<?= base_url($base . "/hapus/" . $d->paket_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>