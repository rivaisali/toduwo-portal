<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <span class="text-muted small"><?= count($data) . "/" . user("undangan"); ?></span>
                <div class="tools dropdown">
                    <?php if (user("undangan") != "0" && count($data) < user("undangan")) { ?>
                        <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                            <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                        </a>
                    <?php } ?>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama Grup</th>
                            <th>Nama Tamu</th>
                            <th>Kontak</th>
                            <th>Keterangan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= ambil_nama_by_id("grup", "nama_grup", "grup_id", $d->grup_id); ?></td>
                                <td><?= $d->nama_lengkap; ?></td>
                                <td><?= $d->email . "<br>" . $d->no_telp; ?></td>
                                <td>
                                    <?= statusTamu($d->status); ?>
                                </td>
                                <td class="actions">
                                    
                                    <?php
$pesan = "*Dear $d->nama_lengkap*
Atas rahmat Tuhan Yang Maha Esa, mohon doa atas syukuran 7 bulanan anak pertama kami:

*Siti Rahayu Ismail, SH & Syaifuddin Yunus, S.Kom*

Kami bermaksud mengundang bapak/ibu/saudara(i) pada acara tasyakuran 7 Bulanan kami yang akan dilaksanakan pada:

*Acara Tasyakuran*
Hari/Tanggal: Sabtu, 06 Februari 2021
Jam: 09:00 Wita - Selesai
Tempat: Rumah Calon Bapak
Alamat : Jalan Taman Surya Kel Dembe 2


*Salam*
*Ayhu & Pudin*

========================
*Kode Undangan Anda: $d->code*
*Dapatkan petunjuk arah & konfirmasi kedatangan anda, serta berikan pesan/doa terbaik anda untuk*
*pasangan Ayhu & Pudin melalui link berikut:* 

https://pudinayujourney.toduwo.id/$d->code

*Pesan ini dikirim melalui toduwo. id*
Made with somewhere in the world";
?>


<?php
if($d->user_id==16){ ?>

<a class="btn btn-space btn-warning" href="https://pudinayujourney.toduwo.id/<?=$d->code;?>" target="_blank">
                                        <i class="mdi mdi-file-check"></i>&nbsp;Lihat Undangan
                                    </a>
                                <a class="btn btn-space btn-success" href="https://wa.me/<?=$no_telp;?>?text=<?=urlencode($pesan);?>">
                                        <i class="mdi mdi-whatsapp"></i>&nbsp;Kirim Whatsapp
                                    </a>
                                    <?php } ?>
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->tamu_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1" href="<?= base_url($base . "/hapus/" . $d->tamu_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>