<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <?= form_hidden('id', $data->tamu_id); ?>
                <div class="form-group pt-1">
                    <label for="grup">Grup Tamu</label>
                    <select name="grup" id="grup" class="form-control form-control-sm <?= form_error('grup') ? 'is-invalid' : ''; ?>" autofocus>
                        <option value="0" <?= set_select('grup', '0'); ?>>- Pilih -</option>
                        <?php foreach ($grup as $g) : ?>
                            <option value="<?= $g->grup_id; ?>" <?= set_select('grup', $g->grup_id, ($g->grup_id == $data->grup_id) ? TRUE : FALSE); ?>>
                                <?= $g->nama_grup; ?>
                            </option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('grup'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="nama">Nama Tamu</label>
                    <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="nama" type="text" placeholder="Nama Tamu Bersama Partner" value="<?= set_value('nama', $data->nama_lengkap); ?>">
                    <?= form_error('nama'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="email">Email</label>
                    <input class="form-control form-control-sm <?= form_error('email') ? 'is-invalid' : ''; ?>" name="email" id="email" type="text" placeholder="nama@example.com" value="<?= set_value('email', $data->email); ?>">
                    <?= form_error('email'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="no_telp">Telp / WA</label>
                    <input class="form-control form-control-sm <?= form_error('no_telp') ? 'is-invalid' : ''; ?>" name="no_telp" id="no_telp" type="text" placeholder="Masukkan No Telp" value="<?= set_value('no_telp', $data->no_telp); ?>">
                    <?= form_error('no_telp'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="jumlah_tamu">Jumlah Tamu</label>
                    <input class="form-control form-control-sm <?= form_error('jumlah_tamu') ? 'is-invalid' : ''; ?>" name="jumlah_tamu" id="jumlah_tamu" type="text" placeholder="Masukkan Jumlah Tamu" value="<?= set_value('jumlah_tamu', $data->jumlah_tamu); ?>">
                    <?= form_error('jumlah_tamu'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>