<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama Desain</th>
                            <th>Deskripsi</th>
                            <th>Paket</th>
                            <th>Link</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $d->judul_desain; ?></td>
                                <td>
                                    <?= $d->keterangan; ?>
                                </td>
                                <td>
                                    <?php
                                    $paket = explode(",", $d->paket);
                                    foreach ($paket as $p) :
                                        echo '<span class="badge badge-primary mr-1">' . ambil_nama_by_id("paket", "nama_paket", "paket_id", $p) . '</span>';
                                    endforeach;
                                    ?>
                                </td>
                                <td>
                                    <?= $d->link_demo; ?>
                                </td>
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->desain_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1" href="<?= base_url($base . "/hapus/" . $d->desain_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>