<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group pt-2">
                            <label for="namaDesain">Judul Desain</label>
                            <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="namaDesain" type="text" placeholder="Masukkan Judul Desain" value="<?= set_value('nama', ''); ?>" autofocus>
                            <?= form_error('nama'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="gambar">Gambar</label>
                            <div class="row">
                                <div class="col-6 col-lg-4">
                                    <img src="<?= base_url('uploads/desain/default.jpg'); ?>" alt="" class="img-thumbnail fotopp-preview">
                                </div>
                                <div class="col-6 col-lg-8">
                                    <!-- <div class="custom-file custom-file-sm"> -->
                                    <input type="file" name="gambar" class="form-control form-control-file form-control-sm <?= form_error('gambar') ? 'is-invalid' : ''; ?> mb-1" id="foto_pp" onchange="previewFotoPp()">
                                    <!-- <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div> -->
                                    <span class="text-muted">Gambar JPG, PNG. Ukuran 10mb</span>
                                    <?= form_error('gambar'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group pt-2">
                            <label for="keteranganPaket">Deskripsi</label>
                            <textarea name="deskripsi" id="keteranganPaket" class="form-control form-control-sm"><?= set_value('deskripsi', ''); ?></textarea>
                        </div>
                        <div class="form-group pt-2">
                            <label for="paket">Paket</label>
                            <select name="paket[]" class="select2 form-control form-control-sm <?= form_error('paket[]') ? 'is-invalid' : ''; ?>" multiple="" data-placeholder="Pilih Paket">
                                <?php foreach ($paket as $p) : ?>
                                    <option value="<?= $p->paket_id; ?>" <?= set_select('paket', $p->paket_id); ?>>
                                        <?= $p->nama_paket; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                            <?= form_error('paket[]'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="linkDemo">Link Demo</label>
                            <input class="form-control form-control-sm <?= form_error('link_demo') ? 'is-invalid' : ''; ?>" name="link_demo" id="linkDemo" type="text" placeholder="Link Demo Desain Undangan" value="<?= set_value('link_demo', ''); ?>" autofocus>
                            <?= form_error('link_demo'); ?>
                        </div>
                    </div>
                    <div class="col-12 pt-3">
                        <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                        <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>