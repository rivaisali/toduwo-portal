<?php
class Group_tamu extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/login');
		}
	}

	private $base = 'group_tamu';
	private $folder = 'group_tamu';

	public function index()
	{
		$cek_undangan			=	$this->crud_model->cek_data("undangan", "user_id", user("user_id"));
		if ($cek_undangan) {
			$user					=	$this->crud_model->select_one("user", "user_id", user("user_id"));
			$data['title']			=	"Buat Undangan";
			$data['desain']			=	$this->crud_model->select_like("desain", "paket", $user->paket_id);
			$this->load->view("backend/undangan", $data);
		} else {
			$data['title']			=	"Group Tamu";
			$data['page']			=	$this->folder . "/index";
			$data["data"]			=	$this->crud_model->select_all_where_array("grup", ["user_id" => user("user_id")]);
			$data['base']			=	$this->base;
			$this->load->view("backend/main", $data);
		}
	}

	public function tambah()
	{
		if ($this->form_validation->run("group_tamu") == FALSE) {
			$data['title']			=	"Tambah Group Tamu";
			$data['page']			=	$this->folder . "/tambah";
			$data['base']			=	$this->base;
			$this->load->view("backend/main", $data);
		} else {
			$data	=	[
				"grup_id"		=>	$this->crud_model->cek_id("grup", "grup_id"),
				"user_id"		=>	user("user_id"),
				"nama_grup"		=>	$this->input->post("nama", true),
				"keterangan"	=>	$this->input->post("keterangan", true)
			];
			$simpan = $this->crud_model->insert("grup", $data);
			if ($simpan) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Grup tamu berhasil ditambah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Grup tamu gagal ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}

	// ubah
	public function ubah($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data	=	$this->crud_model->cek_data_where_array("grup", ["grup_id" => $id, "user_id" => user("user_id")]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				if ($this->form_validation->run("group_tamu") == FALSE) {
					$data['data']			=	$this->crud_model->select_one("grup", "grup_id", $id);
					$data['title']			=	"Ubah Group Tamu";
					$data['page']			=	$this->folder . "/ubah";
					$data['base']			=	$this->base;
					$this->load->view("backend/main", $data);
				} else {
					$data	=	[
						"user_id"		=>	user("user_id"),
						"nama_grup"		=>	$this->input->post("nama", true),
						"keterangan"	=>	$this->input->post("keterangan", true)
					];
					$simpan = $this->crud_model->update("grup", $data, "grup_id", $this->input->post("id"));
					if ($simpan) {
						$notifikasi		=	array(
							"status"	=>	"success", "msg"	=>	"Grup tamu berhasil diubah"
						);
					} else {
						$notifikasi		=	array(
							"status"	=>	"danger", "msg"	=>	"Grup tamu gagal diubah"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect($this->base);
				}
			}
		}
	}

	// hapus
	public function hapus($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data	=	$this->crud_model->cek_data_where_array("grup", ["grup_id" => $id, "user_id" => user("user_id")]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				$hapus = $this->crud_model->hapus_id("grup", "grup_id", $id);
				if ($hapus) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Grup tamu berhasil dihapus"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Grup tamu gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect($this->base);
			}
		}
	}
}
