<?php
class Pembayaran extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user') && user("level") != "admin") {
			redirect('/logout');
		}
	}

	private $base = 'pembayaran';
	private $folder = 'pembayaran';

	public function index()
	{
		$data['title']			=	"Pembayaran";
		$data['page']			=	$this->folder . "/index";
		$data["data"]			=	$this->crud_model->select_all_order("bayar", "create_at", "DESC");
		$data['base']			=	$this->base;
		$this->load->view("backend/main", $data);
	}

	public function detail($id = null)
	{
		$cek_data	=	$this->crud_model->cek_data_where_array("bayar", ["kode_transaksi" => $id]);
		if ($id === null || $cek_data) {
			redirect($this->base);
		} else {
			$data['title']			=	"Detail Pembayaran";
			$data['page']			=	$this->folder . "/detail";
			$data['data']			=	$this->crud_model->select_one("bayar", "kode_transaksi", $id);
			$data['user']			=	$this->crud_model->select_one("user", "kode_transaksi", $id);
			$data['base']			=	$this->base;
			$this->load->view("backend/main", $data);
		}
	}

	public function konfirmasi($status = null, $id = null)
	{
		$cek_data	=	$this->crud_model->cek_data_where_array("bayar", ["kode_transaksi" => $id]);
		if ($id === null || $cek_data) {
			redirect($this->base);
		} else {
			if ($status == "verifikasi") {
				$data_bayar	=	["status" => "2"];
				$data_user	=	["status" => "1"];
			} else {
				$data_bayar	=	["status" => "0"];
				$data_user	=	["status" => "0"];
			}
			$simpan	=	$this->crud_model->update("bayar", $data_bayar, "kode_transaksi", $id);
			if ($simpan) {
				$this->crud_model->update("user", $data_user, "kode_transaksi", $id);
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Pembayaran Berhasil Di Konfirmasi"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Pembayaran Gagal Di Konfirmasi"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}
}
