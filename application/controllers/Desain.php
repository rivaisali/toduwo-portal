<?php
class Desain extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user') && user("level") != "admin") {
			redirect('/logout');
		}
	}

	private $base = 'desain';
	private $folder = 'desain';

	public function index()
	{

		$data['title']			=	"Desain";
		$data['page']			=	$this->folder . "/index";
		$data["data"]			=	$this->crud_model->select_all("desain");
		$data['base']			=	$this->base;
		$this->load->view("backend/main", $data);
	}

	public function tambah()
	{
		if ($this->form_validation->run("desain") == FALSE) {
			$data['title']			=	"Tambah Desain";
			$data['page']			=	$this->folder . "/tambah";
			$data['base']			=	$this->base;
			$data['paket']			=	$this->crud_model->select_all("paket");
			$this->load->view("backend/main", $data);
		} else {
			$tmp_paket	=	$this->input->post("paket", true);
			$paket		=	'';
			$a			=	1;
			foreach ($tmp_paket as $p) {
				if ($a > 1) $paket .= ',';
				$paket .= $p;
				$a++;
			}
			$data	=	[
				"desain_id"		=>	$this->crud_model->cek_id("desain", "desain_id"),
				"judul_desain"	=>	$this->input->post("nama", true),
				"keterangan"	=>	$this->input->post("deskripsi", true),
				"paket"			=>	$paket,
				"link_demo"		=>	$this->input->post("link_demo", true),
			];

			$config['upload_path']     = './uploads/desain/';
			$config['allowed_types']   = 'jpg|png|jpeg';
			$config['max_size']       	= 10000;
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('gambar')) {
				$data_file = $this->upload->data();
				$data['gambar']	=	$data_file['file_name'];
			}

			// print_r($data);
			// $this->crud_model->insert("desain", $data);
			$simpan = $this->crud_model->insert("desain", $data);
			if ($simpan) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Desain berhasil ditambah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Desain gagal ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}

	// ubah
	public function ubah($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data	=	$this->crud_model->cek_data_where_array("desain", ["desain_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				if ($this->form_validation->run("desain") == FALSE) {
					$data['data']			=	$this->crud_model->select_one("desain", "desain_id", $id);
					$data['title']			=	"Ubah Desain";
					$data['page']			=	$this->folder . "/ubah";
					$data['base']			=	$this->base;
					$data['paket']			=	$this->crud_model->select_all("paket");
					$this->load->view("backend/main", $data);
				} else {
					$desain		=	$this->crud_model->select_one("desain", "desain_id", $this->input->post("id"));
					$tmp_paket	=	$this->input->post("paket", true);
					$paket		=	'';
					$a			=	1;
					foreach ($tmp_paket as $p) {
						if ($a > 1) $paket .= ',';
						$paket .= $p;
						$a++;
					}
					$data	=	[
						"judul_desain"	=>	$this->input->post("nama", true),
						"keterangan"	=>	$this->input->post("deskripsi", true),
						"paket"			=>	$paket,
						"link_demo"		=>	$this->input->post("link_demo", true),
					];

					$config['upload_path']     = './uploads/desain/';
					$config['allowed_types']   = 'jpg|png|jpeg';
					$config['max_size']       	= 10000;
					$config['max_filename'] 	= '255';
					$config['encrypt_name'] 	= TRUE;

					$this->load->library('upload', $config);
					if ($this->upload->do_upload('gambar')) {
						if ($desain->gambar != "default.jpg") {
							unlink('./uploads/desain/' . $desain->gambar);
						}
						$data_file = $this->upload->data();
						$data['gambar']	=	$data_file['file_name'];
					}

					$simpan = $this->crud_model->update("desain", $data, "desain_id", $this->input->post("id"));
					if ($simpan) {
						$notifikasi		=	array(
							"status"	=>	"success", "msg"	=>	"Desain berhasil diubah"
						);
					} else {
						$notifikasi		=	array(
							"status"	=>	"danger", "msg"	=>	"Desain gagal diubah"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect($this->base);
				}
			}
		}
	}

	// hapus
	public function hapus($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data	=	$this->crud_model->cek_data_where_array("desain", ["desain_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				$desain		=	$this->crud_model->select_one("desain", "desain_id", $id);
				$hapus = $this->crud_model->hapus_id("desain", "desain_id", $id);
				if ($hapus) {
					if ($desain->gambar != "default.jpg") {
						unlink('./uploads/desain/' . $desain->gambar);
					}
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Desain berhasil dihapus"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Desain gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect($this->base);
			}
		}
	}

	// cek file
	public function check_file()
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/x-citrix-jpeg', 'image/png', 'image/x-png', 'image/x-citrix-png');
		$mime = get_mime_by_extension($_FILES['gambar']['name']);
		if (isset($_FILES['gambar']['name']) && $_FILES['gambar']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES['gambar']['size'] > '10485760') {
					$this->form_validation->set_message('check_file', 'Maksimal 10MB');
					return false;
				} else {
					return true;
				}
			} else {
				$this->form_validation->set_message('check_file', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			if ($this->input->post("id")) {
				return true;
			} else {
				$this->form_validation->set_message('check_file', '{field} tidak boleh kosong');
				return false;
			}
		}
	}
}
