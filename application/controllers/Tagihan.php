<?php
class Tagihan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
	}

	public $base	= 'frontend';
	public $konten	= '/frontend';

	public function index($kode_transaksi = null)
	{
		$data['title']		=	"Tagihan";
		if ($kode_transaksi == null) {
			$data['data']	=	[];
		} else {
			$tagihan		=	$this->crud_model->select_one("user", "kode_transaksi", $kode_transaksi);
			$data['data']	=	$tagihan;
			$data['bayar']	=	$this->crud_model->select_one("bayar", "kode_transaksi", $kode_transaksi);
			$data['paket']	=	$this->crud_model->select_one("paket", "paket_id", $data["data"]->paket_id);
			$data['promo']	=	$this->crud_model->select_one("promo", "kode_referal", $tagihan->kode_promo);
		}
		$this->load->view("frontend/tagihan", $data);
	}

	public function bayar()
	{
		$kode_transaksi = $this->input->post("kode_transaksi", true);

		$this->form_validation->set_rules('bank_pengirim', 'Bank Pengirim', 'trim|callback_check_default');
		$this->form_validation->set_rules('nama_pengirim', 'Nama Pengirim', 'trim|required');
		$this->form_validation->set_rules('nominal', 'Nominal', 'trim|required|numeric');
		$this->form_validation->set_rules('bukti_bayar', 'Bukti Bayar', 'callback_file_check');
		$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');

		if ($this->form_validation->run() == FALSE) {

			$notifikasi		=	array(
				"status"	=>	"danger", "msg"	=>	"Ada kesalahan pada form input bukti pembayaran"
			);
			$this->session->set_flashdata("notifikasi", $notifikasi);

			$data['title']		=	"Tagihan";
			$data['data']	=	$this->crud_model->select_one("user", "kode_transaksi", $kode_transaksi);
			$data['paket']	=	$this->crud_model->select_one("paket", "paket_id", $data["data"]->paket_id);
			$this->load->view("frontend/tagihan", $data);
		} else {
			$cek = $this->crud_model->select_one("bayar", "kode_transaksi", $kode_transaksi);
			if (!empty($cek)) {
				unlink('./uploads/bukti_bayar/' . $cek->bukti_bayar);
				$this->crud_model->hapus_id("bayar", "kode_transaksi", $kode_transaksi);
			}

			$data = [
				"id_bayar"			=>	$this->crud_model->cek_id("bayar", "id_bayar"),
				"kode_transaksi"	=>	$kode_transaksi,
				"tagihan"			=>	$this->input->post("tagihan", true),
				"bank_pengirim"		=>	$this->input->post("bank_pengirim", true),
				"nama_pengirim"		=>	$this->input->post("nama_pengirim", true),
				"nominal"			=>	$this->input->post("nominal", true),
				"bank_tujuan"		=>	$this->input->post("bank_tujuan", true),
				"status"			=>	"1",
			];

			$config['upload_path']     = './uploads/bukti_bayar/';
			$config['allowed_types']   = 'jpg|png|jpeg';
			$config['max_size']       	= 10000;
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('bukti_bayar')) {
				$data_file = $this->upload->data();
				$data['bukti_bayar']	=	$data_file['file_name'];
			}

			$simpan	=	$this->crud_model->insert("bayar", $data);
			if ($simpan) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Bukti Bayar Berhasil Diunggah. Kami Akan Mengirimkan Data Akun Anda Ke Email Yang Anda Daftarkan"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Terjadi Kesalahan Saat Mengunggah Bukti Bayar. Ulangi Beberapa Saat Lagi."
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect("tagihan/" . $kode_transaksi);
		}
	}

	public function file_check()
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/x-citrix-jpeg', 'image/png', 'image/x-png', 'image/x-citrix-png');
		$mime = get_mime_by_extension($_FILES['bukti_bayar']['name']);
		if (isset($_FILES['bukti_bayar']['name']) && $_FILES['bukti_bayar']['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				return true;
			} else {
				$this->form_validation->set_message('file_check', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			$this->form_validation->set_message('file_check', 'Bukti Bayar tidak boleh kosong');
			return false;
		}
	}

	// cek option bernilai 0
	function check_default($post_string)
	{
		if ($post_string == '0') {
			$this->form_validation->set_message('check_default', '{field} Belum dipilih');
			return FALSE;
		} else {
			return TRUE;
		}
		//return $post_string == '0' ? FALSE : TRUE;
	}
}
