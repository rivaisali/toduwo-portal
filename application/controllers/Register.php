<?php
class Register extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
	}

	public $base	= 'frontend';
	public $konten	= '/frontend';

	public function index()
	{
		if ($this->session->userdata('user')) {
			redirect('frontend');
		} else {
			if ($this->input->post('daftar')) {
				$this->form_validation->set_rules('paket', 'Paket', 'trim|required|callback_check_default');
				$this->form_validation->set_rules('tipe', 'Tipe', 'trim|required|callback_check_default');
				$this->form_validation->set_rules('nama', 'Nama', 'trim|required');
				$this->form_validation->set_rules('telp', 'Telp', 'trim|required|numeric|max_length[13]');
				$this->form_validation->set_rules('email', 'E-mail', 'trim|valid_email|required|is_unique[user.email]');
				$this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[8]|max_length[30]');
				$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');

				if ($this->form_validation->run() == FALSE) {
					$data['title']		=	"Mendaftar di Toduwo";
					$data['paket']		=	$this->crud_model->select_all("paket");
					$this->load->view("frontend/register", $data);
				} else {
					$kode_referal	=	$this->input->post("kode_promo", true);
					$paket	=	$this->crud_model->select_one("paket", "paket_id", $this->input->post("paket", true));
					$data	=	array(
						"user_id"			=>	$this->crud_model->cek_id("user", "user_id"),
						"kode_transaksi" 	=>	rand(1111111111, 9999999999),
						"paket_id"			=>	$this->input->post("paket", true),
						"type_user"			=>	$this->input->post("tipe", true),
						"nama"				=>	$this->input->post("nama", true),
						"email"				=>	$this->input->post("email", true),
						"no_telp"			=>	$this->input->post("telp", true),
						"password"			=>	password_hash($this->input->post("password", true), PASSWORD_DEFAULT),
						"undangan"			=>	$paket->max_tamu,
						"penerima_tamu"		=>	$paket->user_penerima_tamu,
						"batas_bayar"		=>	date("Y-m-d"),
						// "kode_referal"		=>	$this->input->post("kode_promo", true),
						"status"			=>	"0"
					);
					if ($kode_referal != "") {
				// 		$cek_promo = $this->crud_model->cek_data_where_array("promo", ["kode_referal" => $kode_referal, "kuota >" => "0"]);
				        $cek_promo = $this->crud_model->cek_data_where_array("promo", ["kode_referal" => $kode_referal, "kuota >" => "0", "paket_id" => $data["paket_id"]]);
						if ($cek_promo) {
							$notifikasi		=	array(
								"status"	=>	"danger", "msg"	=>	"Kode Promo Tidak Ditemukan."
							);
							$this->session->set_flashdata("notifikasi", $notifikasi);
							redirect("register");
						}
					}
					// } else {
					$data["kode_promo"]	=	$kode_referal;
					$daftar		=	$this->crud_model->insert("user", $data);
					if ($daftar) {
						$promo			=	$this->crud_model->select_one("promo", "kode_referal", $kode_referal);
						$this->crud_model->update("promo", ["kuota" => $promo->kuota - 1], "kode_referal", $kode_referal);
				// 		$notifikasi		=	array(
				// 			"status"	=>	"success", "msg"	=>	"Anda berhasil mendaftar, Silahkan lakukan pembayaran sesuai dnegan tagihan dibawah ini. Jangan lupa untuk menyimpan Kode Invoice Anda"
				// 		);
        				if ($data["paket_id"] == "1") {
							$notifikasi		=	array(
								"status"	=>	"success", "msg"	=>	"Anda berhasil mendaftar, Kami sedang menyiapkan undangan anda."
							);
							$this->send_telegram("Ada pendaftaran baru dengan nama <b>".$this->input->post("nama", true)."</b>");
						} else {
							$notifikasi		=	array(
								"status"	=>	"success", "msg"	=>	"Anda berhasil mendaftar, Silahkan lakukan pembayaran sesuai dnegan tagihan dibawah ini. Jangan lupa untuk menyimpan Kode Invoice Anda"
							);
						}
						$this->load->model("email_model");
						$content = 'Anda berhasil terdaftar di <span>toduwo.id</span>. Kode transaksi anda <span>'.$data["kode_transaksi"].'</span> gunakan kode diatas untuk cek tagihan.<br><br>';
						$content .= 'atau klik <a href="'.base_url("tagihan/".$data["kode_transaksi"]).'">Disini.</a>';
						$this->email_model->kirim_email($data["email"], "Registrasi", $content);
					} else {
						$notifikasi		=	array(
							"status"	=>	"danger", "msg"	=>	"Gagal Mendaftar"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect("tagihan/" . $data["kode_transaksi"]);
					// }
				}
			} else {
				$data['title']		=	"Mendaftar di Toduwo";
				$data['paket']		=	$this->crud_model->select_all("paket");
				$this->load->view("frontend/register", $data);
			}
		}
	}

	function check_default($post_string)
	{
		if ($post_string == '0') {
			$this->form_validation->set_message('check_default', '{field} Belum dipilih');
			return FALSE;
		} else {
			return TRUE;
		}
		//return $post_string == '0' ? FALSE : TRUE;
	}
	
	public function send_telegram($msg){
	$token = "1531265288:AAGTjottqDKIVhmZIrE51-Z9xiRQCc8f968";
    $chat_id = "-329803172";
    $hs = $this->http_request("https://api.telegram.org/bot".$token."/sendMessage?chat_id=".$chat_id."&text=".rawurlencode(utf8_encode($msg))."&parse_mode=html");
  }
	
	function http_request($url){
    $ch = curl_init(); 
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    $output = curl_exec($ch); 
    curl_close($ch);      
    return $output;
}
	

}
