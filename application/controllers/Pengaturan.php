<?php
class Pengaturan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/login');
		}
	}

	private $base = 'pengaturan';
	private $folder = 'pengaturan';

	public function index()
	{
		$cek_undangan			=	$this->crud_model->cek_data("undangan", "user_id", user("user_id"));
		if ($cek_undangan) {
			$user					=	$this->crud_model->select_one("user", "user_id", user("user_id"));
			$data['title']			=	"Buat Undangan";
			$data['desain']			=	$this->crud_model->select_like("desain", "paket", $user->paket_id);
			$this->load->view("backend/undangan", $data);
		} else {
			redirect("backend");
		}
	}

	// pengantin
	public function pengantin()
	{
		$cek_data	=	user("user_id");
		if (!$cek_data) {
			redirect("logout");
		} else {
			if ($this->form_validation->run("pengantin_ubah") == FALSE) {
				$data['data']			=	$this->crud_model->select_one("undangan", "user_id", user("user_id"));
				$data['title']			=	"Ubah Data Pengantin";
				$data['page']			=	$this->folder . "/pengantin";
				$data['base']			=	$this->base;
				$this->load->view("backend/main", $data);
			} else {
				$undangan			=	$this->crud_model->select_one("undangan", "user_id", user("user_id"));
				$data	=	[
					"judul"						=>	$this->input->post("judul", true),
					"nama_pengantin_pria"		=>	$this->input->post("nama_pp", true),
					"nama_pengantin_wanita"		=>	$this->input->post("nama_pw", true),
					"tentang_pengantin_pria"	=>	$this->input->post("tentang_pp", true),
					"tentang_pengantin_wanita"	=>	$this->input->post("tentang_pw", true)
				];

				$config['upload_path']     = './uploads/pengantin/';
				$config['allowed_types']   = 'jpg|png';
				$config['max_size']       	= 10000;
				$config['max_filename'] 	= '255';
				$config['encrypt_name'] 	= TRUE;

				$this->load->library('upload', $config);
				if ($this->upload->do_upload('foto_pp')) {
					if ($undangan->foto_pengantin_pria != "pria.png") {
						unlink('./uploads/pengantin/' . $undangan->foto_pengantin_pria);
					}
					$data_file = $this->upload->data();
					$data['foto_pengantin_pria']	=	$data_file['file_name'];
				}
				if ($this->upload->do_upload('foto_pw')) {
					if ($undangan->foto_pengantin_wanita != "wanita.jpg") {
						unlink('./uploads/pengantin/' . $undangan->foto_pengantin_wanita);
					}
					$data_file = $this->upload->data();
					$data['foto_pengantin_wanita']	=	$data_file['file_name'];
				}

				$simpan = $this->crud_model->update("undangan", $data, "undangan_id", $this->input->post("id"));
				if ($simpan) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Data pengantin berhasil diubah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Data pengantin gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("backend");
			}
		}
	}

	public function acara()
	{
		$data['data']			=	$this->crud_model->select_all_where_order("acara", "user_id", user("user_id"), "acara_id", "ASC");
		$data['title']			=	"Acara";
		$data['page']			=	$this->folder . "/acara";
		$data['base']			=	$this->base;
		$this->load->view("backend/main", $data);
	}

	// tambah acara
	public function tambah_acara()
	{
		$cek_data	=	user("user_id");
		if (!$cek_data) {
			redirect("backend");
		} else {
			if ($this->form_validation->run("acara_ubah") == FALSE) {
				$data['title']			=	"Tambah Data Acara";
				$data['undangan']		=	$this->crud_model->select_one("undangan", "user_id", user("user_id"));
				$data['page']			=	$this->folder . "/tambah_acara";
				$data['base']			=	$this->base;
				$this->load->view("backend/main", $data);
			} else {
				$pisah_tgl	=	explode(" - ", $this->input->post("tgl"));

				$data_acara	=	[
					"acara_id"		=>	$this->crud_model->cek_id("acara", "acara_id"),
					"undangan_id"	=>	$this->input->post("undangan_id", true),
					"nama_acara"	=>	$this->input->post("nama_acara", true),
					"tanggal"		=>	$pisah_tgl[0],
					"jam"			=>	$pisah_tgl[1],
					"nama_lokasi"	=>	$this->input->post("lokasi", true),
					"alamat_lokasi"	=>	$this->input->post("alamat", true),
					"user_id"		=>	user("user_id")
				];

				$simpan = $this->crud_model->insert("acara", $data_acara);
				if ($simpan) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Data acara berhasil ditambah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Data acara gagal ditambah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("pengaturan/acara");
			}
		}
	}

	// ubah acara
	public function ubah_acara($id = null)
	{
		$cek_data	=	$this->crud_model->cek_data("acara", "acara_id", $id);
		if ($cek_data) {
			redirect("backend");
		} else {
			if ($this->form_validation->run("acara_ubah") == FALSE) {
				$data['undangan']		=	$this->crud_model->select_one("undangan", "user_id", user("user_id"));
				$data['data']			=	$this->crud_model->select_one("acara", "acara_id", $id);
				$data['title']			=	"Ubah Data Acara";
				$data['page']			=	$this->folder . "/ubah_acara";
				$data['base']			=	$this->base;
				$this->load->view("backend/main", $data);
			} else {
				$pisah_tgl	=	explode(" - ", $this->input->post("tgl"));

				$data_acara	=	[
					"nama_acara"	=>	$this->input->post("nama_acara", true),
					"tanggal"		=>	$pisah_tgl[0],
					"jam"			=>	$pisah_tgl[1],
					"nama_lokasi"	=>	$this->input->post("lokasi", true),
					"alamat_lokasi"	=>	$this->input->post("alamat", true)
				];

				$simpan = $this->crud_model->update("acara", $data_acara, "acara_id", $this->input->post("id"));
				if ($simpan) {
					// $this->crud_model->update("undangan", ["judul" => $this->input->post("judul", true)], "user_id", user("user_id"));
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Data acara berhasil diubah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Data acara gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("pengaturan/acara");
			}
		}
	}

	// akun
	public function akun()
	{
		$cek_data	=	user("user_id");
		if (!$cek_data) {
			redirect("logout");
		} else {
			if ($this->form_validation->run("password_ubah") == FALSE) {
				$data['data']			=	$this->crud_model->select_one("user", "user_id", user("user_id"));
				$data['title']			=	"Akun Saya";
				$data['page']			=	$this->folder . "/akun";
				$data['base']			=	$this->base;
				$this->load->view("backend/main", $data);
			} else {

				$data	=	[
					"password"	=>	password_hash($this->input->post("password_baru", true), PASSWORD_DEFAULT)
				];

				$simpan = $this->crud_model->update("user", $data, "user_id", user("user_id"));
				if ($simpan) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Password berhasil diubah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Password gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("pengaturan/akun");
			}
		}
	}

	// cek file
	public function check_file($str, $name)
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/x-citrix-jpeg', 'image/png', 'image/x-png', 'image/x-citrix-png');
		$mime = get_mime_by_extension($_FILES[$name]['name']);
		if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES[$name]['size'] > '10485760') {
					$this->form_validation->set_message('check_file', 'Maksimal 10MB');
					return false;
				} else {
					return true;
				}
			} else {
				$this->form_validation->set_message('check_file', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			return true;
		}
	}

	// cek tanggal acara
	function tgl_check($post_string)
	{
		// if ($post_string == '0') {
		// 	$this->form_validation->set_message('subdomain_check', '{field} Belum dipilih');
		// 	return FALSE;
		// } else {
		return TRUE;
		// }
	}

	// cek password lama
	function passlama_check($post_string)
	{
		if (password_verify($post_string, user("password"))) {
			return TRUE;
		} else {
			$this->form_validation->set_message('passlama_check', '{field} Tidak Sesuai');
			return FALSE;
		}
	}
}
