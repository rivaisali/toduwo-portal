<?php
class Tamu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        $this->load->model('users_model');
        if (!$this->session->userdata('user')) {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('/login');
        }
    }

    private $base = 'tamu';
    private $folder = 'tamu';

    public function index()
    {
        $cek_undangan            =    $this->crud_model->cek_data("undangan", "user_id", user("user_id"));
        if ($cek_undangan) {
            $user                    =    $this->crud_model->select_one("user", "user_id", user("user_id"));
            $data['title']           =    "Buat Undangan";
            $data['desain']          =    $this->crud_model->select_like("desain", "paket", $user->paket_id);
            $this->load->view("backend/undangan", $data);
        } else {
            $data['title']           =    "Tamu";
            $data['page']            =    $this->folder . "/index";
            $data["data"]            =    $this->crud_model->select_all_where_array("tamu", ["user_id" => user("user_id")]);
            $data['base']            =    $this->base;
            $this->load->view("backend/main", $data);
        }
    }

    public function tambah()
    {
        if ($this->form_validation->run("tamu") == FALSE) {
            $data['title']           =  "Tambah Tamu";
            $data['page']            =  $this->folder . "/tambah";
            $data['base']            =  $this->base;
            $data['grup']            =  $this->crud_model->select_all_where("grup", "user_id", user("user_id"));
            $this->load->view("backend/main", $data);
        } else {
            $data    =    [
                "tamu_id"       =>  $this->crud_model->cek_id("tamu", "tamu_id"),
                "user_id"       =>  user("user_id"),
                "nama_lengkap"  =>  $this->input->post("nama", true),
                "grup_id"       =>  $this->input->post("grup", true),
                "email"         =>  $this->input->post("email", true),
                "no_telp"       =>  $this->input->post("no_telp", true),
                "code"          =>  generateRandomString(10),
                "status"        =>  "0",
                "jumlah_tamu"   =>  $this->input->post("jumlah_tamu", true)
            ];
            $simpan = $this->crud_model->insert("tamu", $data);
            if ($simpan) {
                $notifikasi        =    array(
                    "status"    =>    "success", "msg"    =>    "Tamu berhasil ditambah"
                );
            } else {
                $notifikasi        =    array(
                    "status"    =>    "danger", "msg"    =>    "Tamu gagal ditambah"
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // ubah
    public function ubah($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data    =    $this->crud_model->cek_data_where_array("tamu", ["tamu_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("tamu") == FALSE) {
                    $data['data']            =  $this->crud_model->select_one("tamu", "tamu_id", $id);
                    $data['title']           =  "Ubah Tamu";
                    $data['page']            =  $this->folder . "/ubah";
                    $data['base']            =  $this->base;
                    $data['grup']            =  $this->crud_model->select_all_where("grup", "user_id", user("user_id"));
                    $this->load->view("backend/main", $data);
                } else {
                    $data    =    [
                        "user_id"       =>  user("user_id"),
                        "nama_lengkap"  =>  $this->input->post("nama", true),
                        "grup_id"       =>  $this->input->post("grup", true),
                        "email"         =>  $this->input->post("email", true),
                        "no_telp"       =>  $this->input->post("no_telp", true),
                        "jumlah_tamu"   =>  $this->input->post("jumlah_tamu", true)
                    ];
                    $simpan = $this->crud_model->update("tamu", $data, "tamu_id", $this->input->post("id"));
                    if ($simpan) {
                        $notifikasi        =    array(
                            "status"    =>    "success", "msg"    =>    "Tamu berhasil diubah"
                        );
                    } else {
                        $notifikasi        =    array(
                            "status"    =>    "danger", "msg"    =>    "Tamu gagal diubah"
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base);
                }
            }
        }
    }

    // hapus
    public function hapus($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data    =    $this->crud_model->cek_data_where_array("tamu", ["tamu_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("tamu", "tamu_id", $id);
                if ($hapus) {
                    $notifikasi        =    array(
                        "status"    =>    "success", "msg"    =>    "Tamu berhasil dihapus"
                    );
                } else {
                    $notifikasi        =    array(
                        "status"    =>    "danger", "msg"    =>    "Tamu gagal diubah"
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base);
            }
        }
    }

    // cek option bernilai 0
    function check_default($post_string)
    {
        if ($post_string == '0') {
            $this->form_validation->set_message('check_default', '{field} Belum dipilih');
            return FALSE;
        } else {
            return TRUE;
        }
        //return $post_string == '0' ? FALSE : TRUE;
    }
}
