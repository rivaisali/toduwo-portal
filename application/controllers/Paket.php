<?php
class Paket extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user') && user("level") != "admin") {
			redirect('/logout');
		}
	}

	private $base = 'paket';
	private $folder = 'paket';

	public function index()
	{

		$data['title']			=	"Paket";
		$data['page']			=	$this->folder . "/index";
		$data["data"]			=	$this->crud_model->select_all_order("paket", "harga", "ASC");
		$data['base']			=	$this->base;
		$this->load->view("backend/main", $data);
	}

	public function tambah()
	{
		if ($this->form_validation->run("paket") == FALSE) {
			$data['title']			=	"Tambah Paket";
			$data['page']			=	$this->folder . "/tambah";
			$data['base']			=	$this->base;
			$this->load->view("backend/main", $data);
		} else {
			$data	=	[
				"paket_id"				=>	$this->crud_model->cek_id("paket", "paket_id"),
				"nama_paket"			=>	$this->input->post("nama", true),
				"deskripsi"				=>	$this->input->post("deskripsi", true),
				"max_tamu"				=>	$this->input->post("max_tamu", true),
				"user_penerima_tamu"	=>	$this->input->post("user_penerima_tamu", true),
				"harga"					=>	$this->input->post("harga", true)
			];
			$simpan = $this->crud_model->insert("paket", $data);
			if ($simpan) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Paket berhasil ditambah"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Paket gagal ditambah"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}

	// ubah
	public function ubah($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data	=	$this->crud_model->cek_data_where_array("paket", ["paket_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				if ($this->form_validation->run("paket") == FALSE) {
					$data['data']			=	$this->crud_model->select_one("paket", "paket_id", $id);
					$data['title']			=	"Ubah Paket";
					$data['page']			=	$this->folder . "/ubah";
					$data['base']			=	$this->base;
					$this->load->view("backend/main", $data);
				} else {
					$data	=	[
						"nama_paket"			=>	$this->input->post("nama", true),
						"deskripsi"				=>	$this->input->post("deskripsi", true),
						"max_tamu"				=>	$this->input->post("max_tamu", true),
						"user_penerima_tamu"	=>	$this->input->post("user_penerima_tamu", true),
						"harga"					=>	$this->input->post("harga", true)
					];
					$simpan = $this->crud_model->update("paket", $data, "paket_id", $this->input->post("id"));
					if ($simpan) {
						$notifikasi		=	array(
							"status"	=>	"success", "msg"	=>	"Paket berhasil diubah"
						);
					} else {
						$notifikasi		=	array(
							"status"	=>	"danger", "msg"	=>	"Paket gagal diubah"
						);
					}
					$this->session->set_flashdata("notifikasi", $notifikasi);
					redirect($this->base);
				}
			}
		}
	}

	// hapus
	public function hapus($id = null)
	{
		if ($id === null) {
			redirect($this->base);
		} else {
			$cek_data	=	$this->crud_model->cek_data_where_array("paket", ["paket_id" => $id]);
			if ($cek_data) {
				redirect($this->base);
			} else {
				$hapus = $this->crud_model->hapus_id("paket", "paket_id", $id);
				if ($hapus) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Paket berhasil dihapus"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Paket gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect($this->base);
			}
		}
	}
}
