<?php
class Pengguna extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user') && user("level") != "admin") {
			redirect('/logout');
		}
	}

	private $base = 'pengguna';
	private $folder = 'pengguna';

	public function index()
	{
		$data['title']			=	"Pengguna";
		$data['page']			=	$this->folder . "/index";
		$data["data"]			=	$this->crud_model->select_all_where("user", "level", "user");
		$data['base']			=	$this->base;
		$this->load->view("backend/main", $data);
	}

	public function detail($id = null)
	{
		$cek_data	=	$this->crud_model->cek_data_where_array("user", ["user_id" => $id]);
		if ($id === null || $cek_data) {
			redirect($this->base);
		} else {
	    	$data['title']			=	"Detail Pengguna";
			$data['page']			=	$this->folder . "/detail";
			$data['data']			=	$this->crud_model->select_one("user", "user_id", $id);
			$data['undangan']		=	$this->crud_model->select_one("undangan", "user_id", $id);
			$data['acara']			=	$this->crud_model->select_all_where_order("acara", "user_id", $id, "tanggal", "ASC");
			$data['base']			=	$this->base;
			$this->load->view("backend/main", $data);
		}
	}

	public function verifikasi($id = null)
	{
		$cek_data	=	$this->crud_model->cek_data_where_array("user", ["user_id" => $id]);
		if ($id === null || $cek_data) {
			redirect($this->base);
		} else {
			$data	=	[
				"status"		=>	"1"
			];
			$simpan = $this->crud_model->update("user", $data, "user_id", $id);
			if ($simpan) {
				$user 			=	$this->crud_model->select_one("user", "user_id", $id);
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Pengguna berhasil di verifikasi"
				);
				$this->load->model("email_model");
				$content = 'Akun anda telah diaktifkan.<br>';
				$content .= 'klik <a href="' . base_url("login") . '">Disini.</a> untuk login';
				$this->email_model->kirim_email($user->email, "Aktifasi Akun", $content);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Pengguna gagal di verifikasi"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}

	public function blokir($id = null)
	{
		$cek_data	=	$this->crud_model->cek_data_where_array("user", ["user_id" => $id]);
		if ($id === null || $cek_data) {
			redirect($this->base);
		} else {
			$data	=	[
				"status"		=>	"0"
			];
			$simpan = $this->crud_model->update("user", $data, "user_id", $id);
			if ($simpan) {
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Pengguna berhasil di blokir"
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Pengguna gagal di blokir"
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect($this->base);
		}
	}
}
