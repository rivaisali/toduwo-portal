<?php

defined('BASEPATH') or exit('No direct script access allowed');
// $config['first_link']       = 'First';
// $config['last_link']        = 'Last';
// $config['next_link']        = 'Next';
// $config['prev_link']        = 'Prev';
$config['num_links'] = 3;
$config['use_page_numbers'] = TRUE;

$config['full_tag_open']    = '<nav><ul class="pagination justify-content-center">';
$config['full_tag_close']    = '</ul></ul>';

$config['first_link'] = 'First';
$config['first_tag_open']    = '<li class="page-item">';
$config['first_tag_close']    = '</li>';

$config['last_link'] = 'Last';
$config['last_tag_open']    = '<li class="page-item">';
$config['last_tag_close']    = '</li>';

$config['next_link'] = '&raquo';
$config['next_tag_open']    = '<li class="page-item">';
$config['next_tag_close']    = '</li>';

$config['prev_link'] = '&laquo';
$config['prev_tag_open']    = '<li class="page-item">';
$config['prev_tag_close']    = '</li>';

$config['cur_tag_open']    = '<li class="page-item active"><a class="page-link" href="#">';
$config['cur_tag_close']    = '</a></li>';

$config['digit_tag_open']    = '<li class="page-item">';
$config['digit_tag_close']    = '</li>';

$config['attributes'] = array('class' => 'page-link');


// $config['full_tag_close']   = '</ul></nav></div>';
// $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
// $config['num_tag_close']    = '</span></li>';
// $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
// $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
// $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
// $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
// $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
// $config['prev_tagl_close']  = '</span>Next</li>';
// $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
// $config['first_tagl_close'] = '</span></li>';
// $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
// $config['last_tagl_close']  = '</span></li>';
