<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'frontend';
$route['404_override'] = 'e404';
$route['translate_uri_dashes'] = true;

$route['logout'] = 'login/logout';

$route['tagihan/bayar'] = 'tagihan/bayar';
$route['tagihan/(:any)'] = 'tagihan/index/$1';
