<?php

defined('BASEPATH') or exit('No direct script access allowed');

$config = array(
	'group_tamu'	=> [
		[
			'field' => 'nama',
			'label' => 'Nama Group',
			'rules' => 'trim|required'
		],
		[
			'field' => 'keterangan',
			'label' => 'Keterangan',
			'rules' => 'trim'
		]
	],
	'tamu'	=>	[
		[
			'field' =>	'grup',
			'label'	=>	'Grup Tamu',
			'rules'	=>	'trim|required|callback_check_default'
		],
		[
			'field' =>	'nama',
			'label'	=>	'Nama Tamu',
			'rules'	=>	'trim|required'
		],
		[
			'field' =>	'email',
			'label'	=>	'Email',
			'rules'	=>	'trim|valid_email'
		],
		[
			'field' =>	'no_telp',
			'label'	=>	'Telp / WA',
			'rules'	=>	'trim|numeric|min_length[11]|max_length[14]'
		],
		[
			'field' =>	'jumlah_tamu',
			'label'	=>	'Jumlah Tamu',
			'rules'	=>	'trim|required|numeric'
		],
	],
	'penerima_tamu'	=> [
		[
			'field' => 'nama',
			'label' => 'Nama Penerima Tamu',
			'rules' => 'trim|required'
		]
	],
	'pengantin_ubah' => [
		[
			'field'	=>	'judul',
			'label'	=>	'Judul Undangan',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'nama_pp',
			'label'	=>	'Nama Pengantin Pria',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'nama_pw',
			'label'	=>	'Nama Pengantin Wanita',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'tentang_pp',
			'label'	=>	'Tentang Pengantin Pria',
			'rules'	=>	'trim'
		],
		[
			'field'	=>	'tentang_pw',
			'label'	=>	'Tentang Pengantin Wanita',
			'rules'	=>	'trim'
		],
		[
			'field'	=>	'foto_pp',
			'label'	=>	'Foto Pengantin Pria',
			'rules'	=>	'callback_check_file[foto_pp]'
		],
		[
			'field'	=>	'foto_pw',
			'label'	=>	'Foto Pengantin Wanita',
			'rules'	=>	'callback_check_file[foto_pw]'
		],
	],
	'acara_ubah' => [
		[
			'field'	=>	'nama_acara',
			'label'	=>	'Nama Acara',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'tgl',
			'label'	=>	'Tanggal dan Jam',
			'rules'	=>	'trim|required|callback_tgl_check'
		],
		[
			'field'	=>	'lokasi',
			'label'	=>	'Nama Lokasi',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'alamat',
			'label'	=>	'Alamat Lokasi',
			'rules'	=>	'trim|required'
		]
	],
	'password_ubah' => [
		[
			'field'	=>	'password_lama',
			'label'	=>	'Password Lama',
			'rules'	=>	'trim|required|callback_passlama_check'
		],
		[
			'field'	=>	'password_baru',
			'label'	=>	'Password Baru',
			'rules'	=>	'trim|required|min_length[8]'
		],
		[
			'field'	=>	'ulangi_password_baru',
			'label'	=>	'Ulangi Password Baru',
			'rules'	=>	'trim|required|matches[password_baru]'
		],
	],
	'paket' => [
		[
			'field'	=>	'nama',
			'label'	=>	'Nama Paket',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'deskripsi',
			'label'	=>	'Deskripsi',
			'rules'	=>	'trim'
		],
		[
			'field'	=>	'max_tamu',
			'label'	=>	'Maximal Tamu',
			'rules'	=>	'trim|required|numeric'
		],
		[
			'field'	=>	'user_penerima_tamu',
			'label'	=>	'Penerima Tamu',
			'rules'	=>	'trim|required|numeric'
		],
		[
			'field'	=>	'harga',
			'label'	=>	'Harga',
			'rules'	=>	'trim|required|numeric'
		],
	],
	'desain' => [
		[
			'field'	=>	'nama',
			'label'	=>	'Nama Desain',
			'rules'	=>	'trim|required'
		],
		[
			'field'	=>	'link_demo',
			'label'	=>	'Link Demo',
			'rules'	=>	'trim'
		],
		[
			'field'	=>	'deskripsi',
			'label'	=>	'Deskripsi',
			'rules'	=>	'trim'
		],
		[
			'field'	=>	'gambar',
			'label'	=>	'Gambar',
			'rules'	=>	'callback_check_file'
		],
		[
			'field'	=>	'paket[]',
			'label'	=>	'Paket',
			'rules'	=>	'trim|required'
		],
	]
);

$config['error_prefix'] = '<span class="clearfix invalid-feedback">';
$config['error_sufix'] =  '</span>';





/* End of file form_validation.php */

/* Location: ./application/config/form_validation.php */
